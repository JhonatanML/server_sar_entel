﻿using Modelo;
using Modelo.BEANS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Controlador
{
    public class ControladorUsuario
    {
        public static BeanUsuario validarUsuario(String login, String clave)
        {
            BeanUsuario bean = new BeanUsuario();
            DataTable dt = MUsuario.validarUsuario(login, clave);

            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];

                bean.id = row["ID"].ToString();
                bean.nombre = row["NOMBRE_USUARIO"].ToString();
                bean.login = row["USUARIO"].ToString();
                //bean.perfil = row["PERFIL"].ToString();
                bean.estado = row["ESTADO"].ToString();
                bean.id_perfil = row["id_perfil"].ToString();
            }

            return bean;
        }

        public static List<BeanPermisos> permisosUsuario(String idPerfil, String idUsuario)
        {
            DataTable dt = MUsuario.permisosUsuario(idPerfil, idUsuario);
            List<BeanPermisos> lista = new List<BeanPermisos>();

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    BeanPermisos bean = new BeanPermisos();
                    bean.idPermiso = Convert.ToInt32(row["IDPERMISO"]);
                    bean.NombreOpcion = row["NOMBREOPCION"].ToString();
                    bean.Url = row["URL"].ToString();
                    bean.NombreCategoria = row["NOMBRECATEGORIA"].ToString();
                    bean.Referencia = row["REFERENCIA"].ToString();
                    lista.Add(bean);
                }
            }
            return lista;
        }

        public static DataTable ObtenerConsultores(Int32 piIdUsuario, String psCodigoPerfil, String piIdZona)
        {
            return MUsuario.ObtenerConsultores(piIdUsuario, psCodigoPerfil, piIdZona);
        }
        
        public static List<BeanUsuario> ObtenerConsultoresDinamico(Int32 piIdUsuario, String psCodigoPerfil, String piIdZona)
        {
            List<BeanUsuario> lst = new List<BeanUsuario>();
            BeanUsuario bean = null;
            DataTable dt = MUsuario.ObtenerConsultores(piIdUsuario, psCodigoPerfil, piIdZona);
            if (dt != null && dt.Rows.Count>0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    bean = new BeanUsuario();
                    bean.id = row["IdConsultor"] == DBNull.Value ? "" : row["IdConsultor"].ToString();
                    bean.nombre = row["DESCRIPCION"] == DBNull.Value ? "" : row["DESCRIPCION"].ToString();
                    lst.Add(bean);
                }
            }

            return lst;
        }
    }
}
