﻿using Modelo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Controlador
{
    public class ControladorRazonSocial
    {
        public static DataTable ObtenerRazonSocialBitacora(String psRazonSocial)
        {
            return MRazonSocial.ObtenerRazonSocialBitacora(psRazonSocial);
        }
    }
}
