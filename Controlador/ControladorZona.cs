﻿using Modelo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Controlador
{
    public class ControladorZona
    {
        public static DataTable ObtenerComboZona()
        {
            return MZona.ObtenerComboZona();
        }
    }
}
