﻿using Modelo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Controlador
{
    public class ControladorTipoAtencion
    {
        public static DataTable ObtenerComboTipoAtencion()
        {
            return MTipoAtencion.ObtenerComboTipoAtencion();
        }
    }
}
