﻿using Modelo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Controlador
{
    public class ControladorCliente
    {
        public static DataTable fnSelRazonSocial(String psRazonSocial, String idConsultor)
        {
            return MCliente.fnSelRazonSocial(psRazonSocial, idConsultor);
        }
    }
}
