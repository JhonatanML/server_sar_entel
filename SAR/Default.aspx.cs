﻿using Controlador;
using Modelo.BEANS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilidad;

namespace SAR
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String accion = Request["acc"];

            if (accion == null)
            { accion = ""; }


            if (accion.Equals("SES"))
            {
                this.txtmsg.Text = "<script>window.onload = function() {addnotify(\"alert\", \"Su sesion ha expirado, ingrese nuevamente.\", \"usernregister\");};</script>";
            }

            if (accion.Equals("EXT"))
            {
                this.txtmsg.Text = "<script>window.onload = function() {addnotify(\"alert\", \"Ha cerrado sesión satistactoriamente.\", \"usernregister\");};</script>";
            }
            if (accion.Equals("ERR"))
            {
                this.txtmsg.Text = "<script>window.onload = function() {addnotify(\"alert\", \"Ocurrio un Error, Revise Log Generado.\", \"usernregister\");};</script>";
            }
        }
        protected void btnIngresar_Click(object sender, EventArgs e)
        {
            try
            {
                String login = this.txtUsuario.Text;
                String clave = this.txtClave.Text;
                if (login != "" && clave != "")
                {
                    BeanUsuario bean = ControladorUsuario.validarUsuario(login, clave);
                    if (bean.estado == "0")//USUARIO ESTA DESACTIVADO
                    {
                        this.txtmsg.Text = "<script>window.onload = function() {addnotify(\"alert\", \"Usuario esta Desactivado\", \"usernregister\");};</script>";
                    }
                    else
                    {
                        if (bean.id != null)
                        {
                            //GUARDO TODOS LOS PARAMETROS DEL USUARIO LOGUEADO EN LA SESION
                            Session[Configuracion.SESSION_OBJ_USUARIO] = bean;
                            Response.Redirect("Menu.aspx");
                        }
                        else
                        {
                            this.txtmsg.Text = "<script>window.onload = function() {addnotify(\"alert\", \"Usuario o contraseña incorrecta, Tenga en cuenta Mayus./Minus.\", \"usernregister\");};</script>";
                        }
                    }
                }
                else
                {
                    this.txtmsg.Text = "<script>window.onload = function() {addnotify(\"alert\", \"Ingrese Usuario/Contraseña\", \"usernregister\");};</script>";
                }
            }
            catch (Exception ex)
            {
                Utility.registrarLog("< ERROR [Default.aspx.CS]:> " + ex.Message);
                HttpContext.Current.Response.Write(ExceptionUtils.getHtmlErrorPage(ex));
                HttpContext.Current.Response.End();
            }

        }
    }
}