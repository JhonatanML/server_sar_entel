/// <reference path="../Mantenimiento/Clientes/Editar.aspx" />
function alertHtml(tipo, error, titulo) {
    var strHtml = '';
    if (tipo == "delConfirm") {
        strHtml = '<div class="modal-header">';
        strHtml = strHtml + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
        strHtml = strHtml + '<h3 id="H2">Alerta</h3></div>';
        strHtml = strHtml + '<div class="modal-body" style="background:white">';
        strHtml = strHtml + '<img src="../../images/alert/ico_alert.png" style="float: left;height: 32px;"/> <p style="float: left;line-height: 32px;margin-left: 10px!important;">' + titulo + '</p></div>';
        strHtml = strHtml + '<div class="modal-footer" style="background:#e5e5e5">';
        strHtml = strHtml + '<button class="form-button cz-form-content-input-button" data-dismiss="modal" aria-hidden="true" style="margin-right: 10px;">NO</button>'
        strHtml = strHtml + '<button class="btnDelSi form-button cz-form-content-input-button" data-dismiss="modal" aria-hidden="true">SI</button></div>';
    }
    else if (tipo == "delConfirmFalta") {
        strHtml = '<div class="modal-header">';
        strHtml = strHtml + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
        strHtml = strHtml + '<h3 id="H1">Alerta</h3></div>';
        strHtml = strHtml + '<div class="modal-body" style="background:white">';
        strHtml = strHtml + '<img src="../../images/alert/ico_alert.png" style="float: left;height: 32px;"/> <p style="float: left;line-height: 32px;margin-left: 10px!important;">  Seleccione por lo menos un item.</p></div>';
        strHtml = strHtml + '<div class="modal-footer" style="background:#e5e5e5">';
        strHtml = strHtml + '<button class="form-button cz-form-content-input-button" data-dismiss="modal" aria-hidden="true">CERRAR</button></div>';
    }
    else if (tipo == "error") {
        strHtml = '<div class="modal-header">';
        strHtml = strHtml + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
        strHtml = strHtml + '<h3 id="H1">Error</h3></div>';
        strHtml = strHtml + '<div class="modal-body" style="background:white">';
        strHtml = strHtml + error + '</div>';
        strHtml = strHtml + '<div class="modal-footer" style="background:#e5e5e5">';
        strHtml = strHtml + '<button class="form-button cz-form-content-input-button" data-dismiss="modal" aria-hidden="true">CERRAR</button></div>';
    }
    else if (tipo == "cargando") {
        strHtml = '<div class="loading" id="loading"></div>';
        strHtml = strHtml + '<div style="text-align: center;"><span style="color:white;font-size: 16px;">';
        strHtml = strHtml + titulo;
        strHtml = strHtml + '</span></div>';
    }
    else if (tipo == "delConfirmCUSTOM") {
        strHtml = '<div class="modal-header">';
        strHtml = strHtml + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
        strHtml = strHtml + '<h3 id="H2">Confirmación</h3></div>';
        strHtml = strHtml + '<div class="modal-body" style="background:white">';
        strHtml = strHtml + '<img src="../../images/alert/ico_alert.png" style="float: left;height: 32px;"/> <p style="float: left;line-height: 32px;margin-left: 10px!important;">' + titulo + '</p></div>';
        strHtml = strHtml + '<div class="modal-footer" style="background:#e5e5e5">';
        strHtml = strHtml + '<button class="form-button cz-form-content-input-button" data-dismiss="modal" aria-hidden="true" style="margin-right: 10px;">NO</button>'
        strHtml = strHtml + '<button class="btnDelSi form-button cz-form-content-input-button" aria-hidden="true">SI</button></div>';
    }
    else if (tipo == "errorCarga") {
        strHtml = '<div class="modal-header">';
        strHtml = strHtml + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
        strHtml = strHtml + '<h3 id="H1">' + titulo + '</h3></div>';
        strHtml = strHtml + '<div class="modal-body" style="background:white">';
        strHtml = strHtml + error + '</div>';
        strHtml = strHtml + '<div class="modal-footer" style="background:#e5e5e5">';
        strHtml = strHtml + '<button class="form-button cz-form-content-input-button" data-dismiss="modal" aria-hidden="true">CERRAR</button></div>';
    }
    else if (tipo == "alertValidacion") {
        strHtml = '<div class="modal-header">';
        strHtml = strHtml + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
        strHtml = strHtml + '<h3 id="H1">Alerta</h3></div>';
        strHtml = strHtml + '<div class="modal-body" style="background:white">';
        strHtml = strHtml + '<img src="../../images/alert/ico_alert.png" style="float: left;height: 32px;"/> <p style="float: left;margin-top: 6px!important;margin-left: 7px!important;">' + error + '</div>';
        strHtml = strHtml + '<div class="modal-footer" style="background:#e5e5e5">';
        strHtml = strHtml + '<button class="form-button cz-form-content-input-button" data-dismiss="modal" aria-hidden="true">CERRAR</button></div>';
    } else if (tipo == "alertDetalleURL") {
        strHtml = '<div class="modal-header">';
        strHtml = strHtml + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
        strHtml = strHtml + '<h3 id="H1">' + titulo + '</h3></div>';
        strHtml = strHtml + '<div class="modal-body" style="background:white">';
        strHtml = strHtml + '<p style="text-align: justify;"><a href="' + error + '" style="color: #0066cc;" target="_blank">' + error + '</a></p></div>';
        strHtml = strHtml + '<div class="modal-footer" style="background:#e5e5e5">';
        strHtml = strHtml + '<button class="form-button cz-form-content-input-button" data-dismiss="modal" aria-hidden="true">CERRAR</button></div>';
    } else if (tipo == "alertDetalle") {
        strHtml = '<div class="modal-header">';
        strHtml = strHtml + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
        strHtml = strHtml + '<h3 id="H1">' + titulo + '</h3></div>';
        strHtml = strHtml + '<div class="modal-body" style="background:white;overflow-y: auto;height: 350px;">';

        strHtml = strHtml + '<table style="width: 100%;" class="grilla table table-bordered table-striped">';
        strHtml = strHtml + '<tr><th>Números Enviados</th></tr>';      
        var temp = new Array();
        // this will return an array with strings "1", "2", etc.
        temp = error.split(",");
        for (a in temp) {           

            strHtml = strHtml + '<tr><td>' + temp[a] + '</td></tr>';
        } 
        
        strHtml = strHtml + '</table>';         
         
        strHtml = strHtml + '</div>';
        strHtml = strHtml + '<div class="modal-footer" style="background:#e5e5e5">';
        strHtml = strHtml + '<button class="form-button cz-form-content-input-button" data-dismiss="modal" aria-hidden="true">CERRAR</button></div>';
    }     


    return strHtml;

}
/*Elimina Registros*/
function deleteReg() {

    $('.delReg').click(function (e) {

        var strReg = "";

        $('#divGridView input:checked').each(function () {
            if (this.name != "chkSelectAll")
            { strReg = strReg + this.value + "|"; }
        });

        if (strReg != "") {
            $('#myModal').html(alertHtml('delConfirm', "", "¿Esta seguro que desea borrar los items seleccionados?"));
            $('#myModal').modal('show');

            $('.btnDelSi').click(function (e) {

                var strReg = "";

                $('#divGridView input:checked').each(function () {
                    if (this.name != "chkSelectAll")
                    { strReg = strReg + this.value + "|"; }
                });
                var strData = new Object();
                strData.codigos = strReg;


                $.ajax({
                    type: 'POST',
                    url: urldel,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    data: JSON.stringify(strData),
                    success: function (data) {
                        addnotify("notify", "Registros Eliminados Correctamente.", "registeruser");
                        busqueda();
                        //$('#buscar').trigger("click");

                    },
                    error: function (xhr, status, error) {
                        $("#myModal").html(alertHtml('error', xhr.responseText));
                        $('#myModal').modal('show');
                    }
                });
            });

        }
        else {
            $('#myModal').html(alertHtml('delConfirmFalta'));
            $('#myModal').modal('show');
        }


    });

    //SD

    $('.delItemReg').live('click', function (e) {
        var strReg = $(this).attr('cod');
        var strEstado = $(this).attr('estado');
        $('#myModal').html(alertHtml('delConfirm', "", "¿Esta seguro que desea actualizar el estado de este usuario?"));
        $('#myModal').modal('show');
        $('.btnDelSi').click(function (e) {
            var strData = new Object();
            strData.codigo = strReg;
            strData.estado = strEstado;
            if (strData.estado == 1) {
                $.ajax({
                    type: 'POST',
                    url: urlUpdEstado,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    data: JSON.stringify(strData),
                    success: function (data) {
                        addnotify("notify", "Usuario deshabilitado", "registeruser");
                        busqueda();
                        //$('#buscar').trigger("click");
                    },
                    error: function (xhr, status, error) {
                        $("#myModal").html(alertHtml('error', xhr.responseText));
                        $('#myModal').modal('show');
                    }
                });
            }
            else if (strData.estado == 0) {
                $.ajax({
                    type: 'POST',
                    url: urlUpdEstado,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    data: JSON.stringify(strData),
                    success: function (data) {
                        addnotify("notify", "Usuario habilitado", "registeruser");
                        busqueda();
                        //$('#buscar').trigger("click");
                    },
                    error: function (xhr, status, error) {
                        $("#myModal").html(alertHtml('error', xhr.responseText));
                        $('#myModal').modal('show');
                    }
                });
            }
        });
    });

    function busqueda() {
        var strData = getParametros();
        strData.pagina = $('#hdnActualPage').val();
        $.ajax({
            type: 'POST',
            url: urlbususu,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $('.form-gridview-data').html('');
                $('.form-gridview-error').html('');
                $('.form-gridview-search').show();
            },
            success: function (data) {
                $('.form-gridview-search').hide();
                $('.form-gridview-error').html('');
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
            },
            error: function (xhr, status, error) {
                $('.form-gridview-search').hide();
                $('.form-gridview-data').html();
                $('.form-gridview-error').html("<p><strong>ERROR: </strong> Se ha producido un error en el codigo. Para mayor detalle </p><a id='divError'>haga click aqui</a>");
                $("#myModal").html(alertHtml('error', xhr.responseText));

                $('#divError').click(function (e) {
                    $('#myModal').modal('show');
                });
            }
        });
    }

}
/*Objetivos*/
function cancelarObjetivo() {
    $('.cancelar').live('click', function (e) {
        $('#myModal').html(alertHtml('delConfirm', "", "No se guardará ningún cambio, ¿Desea continuar?"));
        $('#myModal').modal('show');

        $('.btnDelSi').click(function (e) {
            $('#busqueda').trigger("click");
        });

    });
}
/*Agrega Registros*/
function addReg() {
    $('.addReg').click(function (e) {
        $.ajax({
            type: 'POST',
            url: urlins,
            success: function (data) {
                $("#myModal").html(data);
                $('#myModal').modal('show');

                $('#saveReg').click(function (e) {
                    e.preventDefault();

                    var validateItems = true;

                    $('.requerid').each(function () {
                        $(this).parent().find('span').remove();
                        if ($(this).val() == "") {

                            $(this).after("<span style='COLOR: #b94a48;FONT-SIZE: 10PX;FONT-WEIGHT: BOLD;'>CAMPO OBLIGATORIO</span>");
                            validateItems = false;
                        }
                    });

                    if (!validateItems) {
                        addnotify("notify", "Ingresar los campos obligatorios", "registeruser");
                    }
                    else {
                        $('#divError').attr('class', "alert fade");
                        var hModal = $("#myModal").height();

                        $.ajax({
                            type: 'POST',
                            url: urlsavN,
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            async: true,
                            cache: false,
                            data: JSON.stringify(getData()),
                            success: function (data) {
                                $('#myModal').modal('hide');
                                addnotify("notify", "Registrado Correctamente.", "registeruser");
                                busqueda()
                                //$('#buscar').trigger("click");

                            },
                            error: function (xhr, status, error) {
                                addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "registeruser");
                            }
                        });

                    }
                });

            },
            error: function (xhr, status, error) {

                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');

            }
        });

    });
    function busqueda() {
        var strData = getParametros();
        strData.pagina = $('#hdnActualPage').val();
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $('.form-gridview-data').html('');
                $('.form-gridview-error').html('');
                $('.form-gridview-search').show();
            },
            success: function (data) {
                $('.form-gridview-search').hide();
                $('.form-gridview-error').html('');
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
            },
            error: function (xhr, status, error) {
                $('.form-gridview-search').hide();
                $('.form-gridview-data').html();
                $('.form-gridview-error').html("<p><strong>ERROR: </strong> Se ha producido un error en el codigo. Para mayor detalle </p><a id='divError'>haga click aqui</a>");
                $("#myModal").html(alertHtml('error', xhr.responseText));

                $('#divError').click(function (e) {
                    $('#myModal').modal('show');
                });
            }
        });
    }


}

function addRegObjetivo() {
    var validateItems = true;

    $('.requerid').each(function () {
        $(this).attr('style', '');
        if ($(this).val() == "") {

            $(this).attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
            //after("<span style='COLOR: #b94a48;FONT-SIZE: 10PX;FONT-WEIGHT: BOLD;'>CAMPO OBLIGATORIO</span>");
            validateItems = false;
        }
    });

    if (!validateItems) {
        addnotify("notify", "Ingresar los campos obligatorios", "registeruser");
    }
    else {
        $.ajax({
            type: 'POST',
            url: urlsavN,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            cache: false,
            data: JSON.stringify(getDatosAdd()),
            beforeSend: function () {
                $("#myModalCargando").html(alertHtml('cargando', '', ''));
                $('#myModalCargando').modal({ backdrop: 'static', keyboard: false, show: true });
            },
            success: function (data) {

                addnotify("notify", "Registrado Correctamente.", "registeruser");
                desHabilitarTxt();
                ocultarBotones();
                $('#btnEditar').removeClass('hide').addClass('show');
                $('#myModalCargando').modal('hide');
            },
            error: function (xhr, status, error) {
                $('#myModalCargando').modal('hide');
                addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "registeruser");
            }
        });
    }
    //            });
    //        } else {
    //            addnotify("alert", "Debe seleccionar PERFIL por favor.", "registeruser");
    //        }
    //    } else {
    //        addnotify("alert", "Debe seleccionar AÑO por favor.", "registeruser");
    //    }
    //} else {
    //    addnotify("alert", "Debe seleccionar MES por favor.", "registeruser");
    //}
}

function addIncidenciaMant() {
    if ($('#txtHoraInicio').val() == '') {
        $('#txtHoraInicio').focus();
        addnotify("notify", "Ingrese Hora de Inicio", "id");
        return;
    }
    if ($('#txtHoraFin').val() == '') {
        $('#txtHoraFin').focus();
        addnotify("notify", "Ingrese Hora Fin", "id");
        return;
    }

    var validateItems = true;

    $('.requerid').each(function () {
        $(this).attr('style', '');
        $(this).parent().find('span').remove();
        if ($(this).val() == "") {
            $(this).attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
            $(this).after("<span style='COLOR: #b94a48;FONT-SIZE: 10PX;FONT-WEIGHT: BOLD;'>CAMPO OBLIGATORIO</span>");
            validateItems = false;
        }
    });
    if (!validateItems) {
        addnotify("notify", "Ingresar los campos obligatorios", "registeruser");
    } else {
        var flagCheckTecnologia = false;
        $("#checkListTecnologia li.active").each(function (idx, li) {
            flagCheckTecnologia = true;
        });
        if (!flagCheckTecnologia) {

            $('#divContenidoModal').html(alertHtml('delConfirm', "", "No ha seleccionado ninguna Técnologia, ¿Desea continuar?"));
            $('#divMyModal').modal('show');

            $('.btnDelSi').click(function (e) {
                registrarIncidencia();
            });
        } else {
            var flagCheckClientes = false;
            $("#divListClientes li.list-group-item").each(function (idx, li) {
                flagCheckClientes = true;
            });
            if (!flagCheckClientes) {

                $('#divContenidoModal').html(alertHtml('delConfirm', "", "No ha seleccionado ningún cliente, ¿Desea continuar?"));
                $('#divMyModal').modal('show');

                $('.btnDelSi').click(function (e) {
                    registrarIncidencia();
                });
            } else {
                registrarIncidencia();
            }
        }
    }
}

function addIncidenciaNave() {
    if ($('#txtHoraInicio').val() == '') {
        $('#txtHoraInicio').focus();
        addnotify("notify", "Ingrese Fecha & Hora de Inicio", "id");
        return;
    }
    if ($('#txtHoraFin').val() == '') {
        $('#txtHoraFin').focus();
        addnotify("notify", "Ingrese Fecha & Hora Fin", "id");
        return;
    }

    var validateItems = true;

    $('.requerid').each(function () {
        $(this).attr('style', '');
        $(this).parent().find('span').remove();
        if ($(this).val() == "") {
            $(this).attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
            $(this).after("<span style='COLOR: #b94a48;FONT-SIZE: 10PX;FONT-WEIGHT: BOLD;'>CAMPO OBLIGATORIO</span>");
            validateItems = false;
        }
    });
    if (!validateItems) {
        addnotify("notify", "Ingresar los campos obligatorios", "registeruser");
    } else {
        var flagCheckTecnologia = false;
        $("#checkListTecnologia li.active").each(function (idx, li) {
            flagCheckTecnologia = true;
        });
        if (!flagCheckTecnologia) {

            $('#divContenidoModal').html(alertHtml('delConfirm', "", "No ha seleccionado ninguna Técnologia, ¿Desea continuar?"));
            $('#divMyModal').modal('show');

            $('.btnDelSi').click(function (e) {
                registrarIncidencia();
            });
        } else {
            var flagCheckClientes = false;
            $("#divListClientes li.list-group-item").each(function (idx, li) {
                flagCheckClientes = true;
            });
            if (!flagCheckClientes) {

                $('#divContenidoModal').html(alertHtml('delConfirm', "", "No ha seleccionado ningún cliente, ¿Desea continuar?"));
                $('#divMyModal').modal('show');

                $('.btnDelSi').click(function (e) {
                    registrarIncidencia();
                });
            } else {
                registrarIncidencia();
            }
        }
    }
}

function addIncidenciaDiaria() {
    var validateItems = true;

    $('.requerid').each(function () {
        $(this).attr('style', '');
        $(this).parent().find('span').remove();
        if ($(this).val() == "") {
            $(this).attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
            $(this).after("<span style='COLOR: #b94a48;FONT-SIZE: 10PX;FONT-WEIGHT: BOLD;'>CAMPO OBLIGATORIO</span>");
            validateItems = false;
        }
    });
    if (!validateItems) {
        addnotify("notify", "Ingresar los campos obligatorios", "registeruser");
    } else {
        registrarIncidencia();

    }
}

function addIncidenciaErrorApp() {
    if ($('#txtFechaRep').val() == '') {
        $('#txtFechaRep').focus();
        addnotify("notify", "Ingrese Fecha de Reporte", "id");
        return;
    }
    if ($('#txtHoraRep').val() == '') {
        $('#txtHoraRep').focus();
        addnotify("notify", "Ingrese Hora de Reporte", "id");
        return;
    }

    var validateItems = true;

    $('.requerid').each(function () {
        $(this).attr('style', '');
        $(this).parent().find('span').remove();
        if ($(this).val() == "") {
            $(this).attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
            $(this).after("<span style='COLOR: #b94a48;FONT-SIZE: 10PX;FONT-WEIGHT: BOLD;'>CAMPO OBLIGATORIO</span>");
            validateItems = false;
        }
    });
    if (!validateItems) {
        addnotify("notify", "Ingresar los campos obligatorios", "registeruser");
    } else {
        registrarIncidencia();

    }
}

function registrarIncidencia() {
    $.ajax({
        type: 'POST',
        url: urlsavN,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        cache: false,
        data: JSON.stringify(getParametros()),
        beforeSend: function () {
            $("#divContenidoModal").html(alertHtml('cargando', '', ''));
            $('#divMyModal').modal({ backdrop: 'static', keyboard: false, show: true });
        },
        success: function (data) {
            addnotify("notify", "Registrado Correctamente.", "registeruser");
            document.location.href = "../../Reporte/BuscadorIncidencia/Incidencia.aspx";
        },
        error: function (xhr, status, error) {
            $('#myModalCargando').modal('hide');
            addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "registeruser");
        }
    });
}
function addClienteIncidencia() {
    $('.addRegClienteInc').click(function (e) {
        $.ajax({
            type: 'POST',
            url: urlAgregarCliente,
            success: function (data) {
                $("#divContenidoModal").html(data);
                $('#divMyModal').modal({ backdrop: 'static', keyboard: false, show: true });

                $('#btnGuardaCliIn').click(function (e) {
                    e.preventDefault();
                    var validateItems = false;
                    $("#divDesSelec li.active").each(function (idx, li) {
                        validateItems = true;
                    })
                    if (!validateItems) {
                        addnotify("notify", "No hay ningun cliente Agregado.", "id");
                    }
                    else {
                        var idCheck = '';
                        $("#divDesSelec li.active").each(function (idx, li) {
                            idCheck = idCheck + $(li).attr('id') + '?' + $(li).text() + '|';
                        })
                        var strData = new Object();
                        strData.idsClientes = idCheck;
                        $.ajax({
                            type: 'POST',
                            url: urlClientesSession,
                            contentType: "application/json; charset=utf-8",
                            async: true,
                            cache: false,
                            data: JSON.stringify(strData),
                            beforeSend: function () {
                                $('#msg').html('<center><img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />Espere un momento por Favor...</center>');
                                $('#btnGuardaCliIn').hide();
                                $('#btnCancelar').hide();
                            },
                            success: function (data) {
                                addnotify("notify", "Cliente(s) Agregado(s) correctamente.", "registeruser");
                                $('#divListClientes').html(data);
                                $('#divMyModal').modal('hide');
                            },
                            error: function (xhr, status, error) {
                                $('#msg').hide();
                                $('#btnGuardaCliIn').show();
                                $('#btnCancelar').show();
                                addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                            }
                        });
                    }
                });

            },
            error: function (xhr, status, error) {
                $("#divContenidoModal").html(alertHtml('error', xhr.responseText));
                $('#divMyModal').modal('show');

            }
        });

    });
}


function addTrabajoIncidencia() {
    $('.addRegTrabajoInc').click(function (e) {
        $.ajax({
            type: 'POST',
            url: urlMostrarTrabajo,
            success: function (data) {
                $("#divContenidoModal").html(data);
                $('#divMyModal').modal({ backdrop: 'static', keyboard: false, show: true });

                $('#btnGuardaTrabajoInc').click(function (e) {
                    e.preventDefault();
                    var validateItems = true;
                    $('#taTrabajosRea').attr('style', '');
                    $('#taDetalle').attr('style', '');
                    if ($('#taTrabajosRea').val() == '') {
                        $('#taTrabajosRea').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese trabajos realizados", "registeruser");
                        validateItems = false;
                    } else if ($('#taDetalle').val() == '') {
                        $('#taDetalle').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese detalles", "registeruser");
                        validateItems = false;
                    }
                    if (validateItems) {
                        $.ajax({
                            type: 'POST',
                            url: urlGridTrabajo,
                            contentType: "application/json; charset=utf-8",
                            async: true,
                            cache: false,
                            data: JSON.stringify(getTrabajo()),
                            beforeSend: function () {
                                $('#msg').html('<center><img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />Espere un momento por Favor...</center>');
                                $('#btnLimpiar').hide();
                                $('#btnGuardaTrabajoInc').hide();
                                $('#btnCancelar').hide();
                            },
                            success: function (data) {
                                addnotify("notify", "Trabajo Agregado correctamente.", "registeruser");
                                $('#divRegistroSeg').html(data);
                                $('#divMyModal').modal('hide');
                            },
                            error: function (xhr, status, error) {
                                $('#msg').html('');
                                $('#btnLimpiar').show();
                                $('#btnGuardaTrabajoInc').show();
                                $('#btnCancelar').show();
                                addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                            }
                        });
                    }
                });

            },
            error: function (xhr, status, error) {
                $("#divContenidoModal").html(alertHtml('error', xhr.responseText));
                $('#divMyModal').modal('hide');

            }
        });

    });
}


function addTrabajoIncidencia_Old() {
    $('.addRegTrabajoInc').click(function (e) {
        $.ajax({
            type: 'POST',
            url: urlMostrarTrabajo,
            success: function (data) {
                $("#divContenidoModal").html(data);
                $('#divMyModal').modal({ backdrop: 'static', keyboard: false, show: true });

                $('#btnGuardaTrabajoInc').click(function (e) {
                    e.preventDefault();
                    var validateItems = true;
                    $('#taTrabajosRea').attr('style', '');
                    $('#taDetalle').attr('style', '');
                    if ($('#taTrabajosRea').val() == '') {
                        $('#taTrabajosRea').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese trabajos realizados", "registeruser");
                        validateItems = false;
                    } else if ($('#taDetalle').val() == '') {
                        $('#taDetalle').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese detalles", "registeruser");
                        validateItems = false;
                    }
                    if (validateItems) {
                        $.ajax({
                            type: 'POST',
                            url: urlGuardarTrabajo,
                            contentType: "application/json; charset=utf-8",
                            async: true,
                            cache: false,
                            data: JSON.stringify(getTrabajo()),
                            beforeSend: function () {
                                $('#msg').html('<center><img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />Espere un momento por Favor...</center>');
                                $('#btnLimpiar').hide();
                                $('#btnGuardaTrabajoInc').hide();
                                $('#btnCancelar').hide();
                            },
                            success: function (data) {
                                addnotify("notify", "Trabajo Agregado correctamente.", "registeruser");
                                $('#divRegistroSeg').html(data);
                                $('#divMyModal').modal('hide');
                            },
                            error: function (xhr, status, error) {
                                $('#msg').html('');
                                $('#btnLimpiar').show();
                                $('#btnGuardaTrabajoInc').show();
                                $('#btnCancelar').show();
                                addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                            }
                        });
                    }
                });

            },
            error: function (xhr, status, error) {
                $("#divContenidoModal").html(alertHtml('error', xhr.responseText));
                $('#divMyModal').modal('hide');

            }
        });

    });
}

function updRegObjetivo() {
    $.ajax({
        type: 'POST',
        url: urlsavU,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        cache: false,
        data: JSON.stringify(getDatosUpd()),
        beforeSend: function () {
            $("#myModalCargando").html(alertHtml('cargando', '', ''));
            $('#myModalCargando').modal({ backdrop: 'static', keyboard: false, show: true });
        },
        success: function (data) {
            $('#myModalCargando').modal('hide');
            desHabilitarTxt();
            ocultarBotones();
            $('#btnEditar').removeClass('hide').addClass('show');
            addnotify("notify", "Actualizado Correctamente.", "registeruser");
        },
        error: function (xhr, status, error) {
            $('#myModalCargando').modal('hide');
            addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "registeruser");
        }
    });
}

function updRegObjetivo_OLD() {
    $('.updReg').live('click', function (e) {
        // var strReg = $(this).attr('cod')
        $('#myModal').html(alertHtml('delConfirm', "", "¿Está Ud. Seguro de Actualizar los Registros?"));
        $('#myModal').modal('show');

        $('.btnDelSi').click(function (e) {
            $.ajax({
                type: 'POST',
                url: urlsavU,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                data: JSON.stringify(getDatosUpd()),
                beforeSend: function () {
                    $("#myModalCargando").html(alertHtml('cargando', '', ''));
                    $('#myModalCargando').modal({ backdrop: 'static', keyboard: false, show: true });
                },
                success: function (data) {
                    $('#myModalCargando').modal('hide');
                    desHabilitarTxt();
                    ocultarBotones();
                    $('#btnEditar').removeClass('hide').addClass('show');
                    addnotify("notify", "Actualizado Correctamente.", "registeruser");
                },
                error: function (xhr, status, error) {
                    $('#myModalCargando').modal('hide');
                    addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "registeruser");
                }
            });
        });
    });
}

function modReg() {

    $('.editItemReg').live('click', function (e) {

        var strReg = $(this).attr('cod')
        var strData = new Object();
        strData.codigo = strReg;

        $.ajax({
            type: 'POST',
            url: urlins,
            data: JSON.stringify(strData),
            success: function (data) {
                $("#myModal").html(data);
                $('#myModal').modal('show');

                $('#saveReg').click(function (e) {
                    e.preventDefault();

                    var validateItems = true;
                    $('.requerid').each(function () {
                        $(this).parent().find('span').remove();
                        if ($(this).val() == "") {
                            $(this).after("<span style='color:#b94a48'>*</span>");
                            validateItems = false;
                        }
                    });
                    if (!validateItems) {
                        $("#mensajeError").text("Ingresar los campos obligatorios");
                        $('#divError').attr('class', "alert fade in");
                        $("#tituloMensajeError").text('ADVERTENCIA');
                    }
                    else {
                        $('#divError').attr('class', "alert fade");

                        var hModal = $("#myModal").height();

                        $.ajax({
                            type: 'POST',
                            url: urlsavE,
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            async: true,
                            cache: false,
                            data: JSON.stringify(getData()),
                            success: function (data) {
                                addnotify("notify", "Actualizado Correctamente.", "registeruser");
                                //$('#divError').attr('class', "alert alert-success");
                                //$("#mensajeError").text("Usuario registrado exitosamente");
                                //$("#tituloMensajeError").text('OK');
                                $('#myModal').modal('hide');
                                busqueda();
                                //$('#buscar').trigger("click");

                            },
                            error: function (xhr, status, error) {
                                $('#divError').attr('class', "alert alert-error");
                                $("#tituloMensajeError").text('ERROR');
                                $("#mensajeError").text(jQuery.parseJSON(xhr.responseText).Message);


                            }
                        });

                    }
                });

            },
            error: function (xhr, status, error) {

                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');

            }
        });

    });
    function busqueda() {
        var strData = getParametros();
        strData.pagina = $('#hdnActualPage').val();
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $('.form-gridview-data').html('');
                $('.form-gridview-error').html('');
                $('.form-gridview-search').show();
            },
            success: function (data) {
                $('.form-gridview-search').hide();
                $('.form-gridview-error').html('');
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
            },
            error: function (xhr, status, error) {
                $('.form-gridview-search').hide();
                $('.form-gridview-data').html();
                $('.form-gridview-error').html("<p><strong>ERROR: </strong> Se ha producido un error en el codigo. Para mayor detalle </p><a id='divError'>haga click aqui</a>");
                $("#myModal").html(alertHtml('error', xhr.responseText));

                $('#divError').click(function (e) {
                    $('#myModal').modal('show');
                });
            }
        });
    }
}
/*Busqueda*/
function busReg() {

    $("input[id$='ChkAll']").live('click', function (e) {
        $(".grilla INPUT[type='checkbox']")
                     .attr('checked', $("input[id$='ChkAll']")
                     .is(':checked'));
    });

    $(".grilla INPUT[type='checkbox']").live('click', function (e) {
        if (!$(this)[0].checked) {
            $("input[id$='ChkAll']").attr("checked", false);
        }
    });


    $('#buscar').click(function (e) {
        //cargando la grilla
        var strData = getParametros();
        strData.pagina = 1;
        console.log('data_: OnBlun');
        $('#hdnActualPage').val();
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $('.form-gridview-data').html('');
                $('.form-gridview-error').html('');
                $('.form-gridview-search').show();
            },
            success: function (data) {
                //console.log('data_: ' + data);
                $('.form-gridview-search').hide();
                $('.form-gridview-error').html('');
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
            },
            error: function (xhr, status, error) {
                $('.form-gridview-search').hide();
                $('.form-gridview-data').html();
                $('.form-gridview-error').html("<p><strong>ERROR: </strong> Se ha producido un error en el codigo. Para mayor detalle </p><a id='divError'>haga click aqui</a>");
                $("#myModal").html(alertHtml('error', xhr.responseText));

                $('#divError').click(function (e) {
                    $('#myModal').modal('show');
                });
            }
        });

    });
    $('.pagina-anterior').live('click', function (e) {

        var strData = getParametros();
        strData.pagina = parseInt(strData.pagina) - 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });

    $('.pagina-siguiente').live('click', function (e) {

        var strData = getParametros();
        strData.pagina = parseInt(strData.pagina) + 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });
}

function buscarApp() {
    $('#buscar').click(function (e) {
        //cargando la grilla
        var strData = getParametros();
        strData.pagina = 1;

        $('#hdnActualPage').val();
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $('.form-gridview-data').html(''); 
                $('.form-gridview-error').html('');
                $('.form-gridview-search').show();
            },
            success: function (data) {
                //alert('data_: ' + data);
                $('.form-gridview-search').hide();
                $('.form-gridview-error').html('');
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
            },
            error: function (xhr, status, error) {
                $('.form-gridview-search').hide();
                $('.form-gridview-data').html();
                $('.form-gridview-error').html("<p><strong>ERROR: </strong> Se ha producido un error en el codigo. Para mayor detalle </p><a id='divError'>haga click aqui</a>");
                $("#myModal").html(alertHtml('error', xhr.responseText));

                $('#divError').click(function (e) {
                    $('#myModal').modal('show');
                });
            }
        });

    });


    $('.pagina-anterior').live('click', function (e) {

        var strData = getParametros();
        strData.pagina = parseInt(strData.pagina) - 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });

    $('.pagina-siguiente').live('click', function (e) {

        var strData = getParametros();
        strData.pagina = parseInt(strData.pagina) + 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });
}


function buscarUsu() {
    $('#buscar').click(function (e) {
        //cargando la grilla
        var strData = getParametros();
        strData.pagina = 1;
        $('#hdnActualPage').val();
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $('.form-gridview-data').html('');
                $('.form-gridview-error').html('');
                $('.form-gridview-search').show();
            },
            success: function (data) {
                //console.log('data_: ' + data);
                $('.form-gridview-search').hide();
                $('.form-gridview-error').html('');
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
            },
            error: function (xhr, status, error) {
                $('.form-gridview-search').hide();
                $('.form-gridview-data').html();
                $('.form-gridview-error').html("<p><strong>ERROR: </strong> Se ha producido un error en el codigo. Para mayor detalle </p><a id='divError'>haga click aqui</a>");
                $("#myModal").html(alertHtml('error', xhr.responseText));

                $('#divError').click(function (e) {
                    $('#myModal').modal('show');
                });
            }
        });

    });


    $('.pagina-anterior').live('click', function (e) {

        var strData = getParametros();
        strData.pagina = parseInt(strData.pagina) - 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });

    $('.pagina-siguiente').live('click', function (e) {

        var strData = getParametros();
        strData.pagina = parseInt(strData.pagina) + 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });
}

function buscarCliente() {
    $('#buscar').click(function (e) {
        //cargando la grilla
        var strData = getParametros();
        strData.pagina = 1;
        $('#hdnActualPage').val();
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $('.form-gridview-data').html('');
                $('.form-gridview-error').html('');
                $('.form-gridview-search').show();
            },
            success: function (data) {
                //console.log('data_: ' + data);
                $('.form-gridview-search').hide();
                $('.form-gridview-error').html('');
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
            },
            error: function (xhr, status, error) {
                $('.form-gridview-search').hide();
                $('.form-gridview-data').html();
                $('.form-gridview-error').html("<p><strong>ERROR: </strong> Se ha producido un error en el codigo. Para mayor detalle </p><a id='divError'>haga click aqui</a>");
                $("#myModal").html(alertHtml('error', xhr.responseText));

                $('#divError').click(function (e) {
                    $('#myModal').modal('show');
                });
            }
        });

    });


    $('.pagina-anterior').live('click', function (e) {

        var strData = getParametros();
        strData.pagina = parseInt(strData.pagina) - 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });

    $('.pagina-siguiente').live('click', function (e) {

        var strData = getParametros();
        strData.pagina = parseInt(strData.pagina) + 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });
}

function buscarClienteRadio() {
    $('#buscar').click(function (e) {
        //cargando la grilla
        var strData = getParametros();
        strData.pagina = 1;
        $('#hdnActualPage').val();
        $.ajax({
            type: 'POST',
            url: urlbusR,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $('.form-gridview-data').html('');
                $('.form-gridview-error').html('');
                $('.form-gridview-search').show();
            },
            success: function (data) {
                //console.log('data_: ' + data);
                $('.form-gridview-search').hide();
                $('.form-gridview-error').html('');
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
            },
            error: function (xhr, status, error) {
                $('.form-gridview-search').hide();
                $('.form-gridview-data').html();
                $('.form-gridview-error').html("<p><strong>ERROR: </strong> Se ha producido un error en el codigo. Para mayor detalle </p><a id='divError'>haga click aqui</a>");
                $("#myModal").html(alertHtml('error', xhr.responseText));

                $('#divError').click(function (e) {
                    $('#myModal').modal('show');
                });
            }
        });

    });


    $('.pagina-anterior').live('click', function (e) {

        var strData = getParametros();
        strData.pagina = parseInt(strData.pagina) - 1;
        $.ajax({
            type: 'POST',
            url: urlbusR,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });

    $('.pagina-siguiente').live('click', function (e) {

        var strData = getParametros();
        strData.pagina = parseInt(strData.pagina) + 1;
        $.ajax({
            type: 'POST',
            url: urlbusR,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });
}

function buscarClienteCheck() {
    $('#buscar').click(function (e) {
        //cargando la grilla
        var strData = getParametros();
        strData.pagina = 1;
        $('#hdnActualPage').val();
        $.ajax({
            type: 'POST',
            url: urlbusC,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $('.form-gridview-data2').html('');
                $('.form-gridview-error2').html('');
                $('.form-gridview-search2').show();
            },
            success: function (data) {
                //console.log('data_: ' + data);
                $('.form-gridview-search2').hide();
                $('.form-gridview-error2').html('');
                $('.form-gridview-data2').html(data);
                $('#hdnActualPage').val(strData.pagina);
            },
            error: function (xhr, status, error) {
                $('.form-gridview-search2').hide();
                $('.form-gridview-data2').html();
                $('.form-gridview-error2').html("<p><strong>ERROR: </strong> Se ha producido un error en el codigo. Para mayor detalle </p><a id='divError'>haga click aqui</a>");
                $("#myModal").html(alertHtml('error', xhr.responseText));

                $('#divError').click(function (e) {
                    $('#myModal').modal('show');
                });
            }
        });

    });


    $('.pagina-anterior').live('click', function (e) {

        var strData = getParametros();
        strData.pagina = parseInt(strData.pagina) - 1;
        $.ajax({
            type: 'POST',
            url: urlbusC,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search2").show();
            },
            success: function (data) {
                $('.form-gridview-data2').html(data);
                $('#hdnActualPage').val(strData.pagina);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });

    $('.pagina-siguiente').live('click', function (e) {

        var strData = getParametros();
        strData.pagina = parseInt(strData.pagina) + 1;
        $.ajax({
            type: 'POST',
            url: urlbusC,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search2").show();
            },
            success: function (data) {
                $('.form-gridview-data2').html(data);
                $('#hdnActualPage').val(strData.pagina);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });
}

function buscarUsuario() {
    $('#buscar').click(function (e) {
        //cargando la grilla
        var strData = getParametros();
        strData.pagina = 1;
        $('#hdnActualPage').val();
        //alert("ENTRO");
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                
                $('.form-gridview-data').html('');
                $('.form-gridview-error').html('');
                $('.form-gridview-search').show();
            },
            success: function (data) {
                //console.log('data_: ' + data);
                $('.form-gridview-search').hide();
                $('.form-gridview-error').html('');
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
            },
            error: function (xhr, status, error) {
                $('.form-gridview-search').hide();
                $('.form-gridview-data').html();
                $('.form-gridview-error').html("<p><strong>ERROR: </strong> Se ha producido un error en el codigo. Para mayor detalle </p><a id='divError'>haga click aqui</a>");
                $("#myModal").html(alertHtml('error', xhr.responseText));

                $('#divError').click(function (e) {
                    $('#myModal').modal('show');
                });
            }
        });

    });


    $('.pagina-anterior').live('click', function (e) {

        var strData = getParametros();
        strData.pagina = parseInt(strData.pagina) - 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });

    $('.pagina-siguiente').live('click', function (e) {

        var strData = getParametros();
        strData.pagina = parseInt(strData.pagina) + 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });
}

function buscarAvanceMetricas() {
    $('#buscar').click(function (e) {
        var strData = getParametros();
        $.ajax({
            type: 'POST',
            url: urlBuscar,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $("#divContenidoModal").html(alertHtml('cargando', '', 'Buscando Avance de Metricas...'));
                $('#divMyModal').modal({ backdrop: 'static', keyboard: false, show: true });
            },
            success: function (data) {
                $('#divMyModal').modal('hide');
                $('#divTblRep').html(data);
            },
            error: function (xhr, status, error) {
                $('#divMyModal').modal('hide');
                $('#divTblRep').html("<p><strong>ERROR: </strong> Se ha producido un error en el codigo. Para mayor detalle </p><a id='divError'>haga click aqui</a>");
                $("#divContenidoModal").html(alertHtml('error', xhr.responseText));

                $('#divError').click(function (e) {
                    $('#divMyModal').modal('show');
                });
            }
        });

    });
}

function busObjetivos() {
    $('#busqueda').click(function (e) {
        var strData = getParametros();
        if ($('#dllMes').val() != '-1') {
            if ($('#dllAnio').val() != '-1') {
                if ($('#dllPerfil').val() != '-1') {
                    $.ajax({
                        type: 'POST',
                        url: urlBus,
                        contentType: "application/json; charset=utf-8",
                        async: true,
                        cache: false,
                        data: JSON.stringify(strData),
                        beforeSend: function () {
                            $('#divResultado').html('<center><span class="glyphicon glyphicon-hourglass" style="color:#FF6702;" aria-hidden="true"></span><span class="sr-only">Load:</span> Espere un Momento Por Favor</center>');
                            $("#myModalCargando").html(alertHtml('cargando', '', ''));
                            $('#myModalCargando').modal({ backdrop: 'static', keyboard: false, show: true });
                        },
                        success: function (data) {
                            $('#myModalCargando').modal('hide');
                            $('#divResultado').html(data);
                            if ($('#flagEncontro').val() == '1') {
                                desHabilitarTxt();
                                ocultarBotones();
                                $('#btnEditar').removeClass('hide').addClass('show');
                            } else {
                                habilitarTxt();
                                ocultarBotones();
                                $('#btnGuardar').removeClass('hide').addClass('show');
                            }
                        },
                        error: function (xhr, status, error) {
                            $('.form-gridview-search').hide();
                            $('.form-gridview-data').html();
                            $('.form-gridview-error').html("<p><strong>ERROR: </strong> Se ha producido un error en el codigo. Para mayor detalle </p><a id='divError'>haga click aqui</a>");
                            $("#myModal").html(alertHtml('error', xhr.responseText));

                            $('#divError').click(function (e) {
                                $('#myModal').modal('show');
                            });
                        }
                    });
                } else {
                    addnotify("alert", "Debe seleccionar PERFIL por favor.", "registeruser");
                }
            } else {
                addnotify("alert", "Debe seleccionar AÑO por favor.", "registeruser");
            }
        } else {
            addnotify("alert", "Debe seleccionar MES por favor.", "registeruser");
        }
    });
}

function detReg(control, urlD) {
    $(control).live('click', function (e) {
        if (!$(this).parent().hasClass("nofoto") && !$(this).parent().hasClass("nogps")) {
            var strReg = $(this).attr('cod');
            var strData = new Object();
            strData.codigo = strReg
            $this = $(this);
            $.ajax({
                type: 'POST',
                url: urlD,
                data: JSON.stringify(strData),
                beforeSend: function () {
                    $("#myModal").html('');
                },
                success: function (data) {
                    //console.log("return data");
                    $("#myModal").html(data);
                    var modali = 0;
                    $("#myModalContent div table").each(function () {
                        modali = 1;
                    });

                    if (modali == 0 && $this.hasClass("detReg")) {
                        $("#myModalContent div").addClass("cz-util-center-text").html("<p>No se registro ningun detalle.<p>");
                        $("#cz-form-modal-exportar").hide();
                    }

                    //$('#myModal').on('shown', function () {
                    //    google.maps.event.trigger(map, "resize");
                    //});

                    $('#myModal').modal('show');

                },
                error: function (xhr, status, error) {

                    $("#myModal").html(alertHtml('error', xhr.responseText));
                    $('#myModal').modal('show');

                }
            });
        }
    });
}

function detRegCustom(control, urlD) {
    $(control).live('click', function (e) {
        if (!$(this).parent().hasClass("nofoto") && !$(this).parent().hasClass("nogps")) {
            var strReg = $(this).attr('cod');
            var strData = new Object();
            strData.codigo = strReg
            $this = $(this);
            $.ajax({
                type: 'POST',
                url: urlD,
                data: JSON.stringify(strData),
                beforeSend: function () {
                    $("#myModal2").html('');
                },
                success: function (data) {
                    //console.log("return data");
                    $("#myModal2").html(data);
                    var modali = 0;
                    $("#myModalContent div table").each(function () {
                        modali = 1;
                    });

                    if (modali == 0 && $this.hasClass("detReg")) {
                        $("#myModalContent div").addClass("cz-util-center-text").html("<p>No se registro ningun detalle.<p>");
                        $("#cz-form-modal-exportar").hide();
                    }

                    //$('#myModal2').on('shown', function () {
                    //    google.maps.event.trigger(map, "resize");
                    //});

                    $('#myModal2').modal('show');

                },
                error: function (xhr, status, error) {

                    $("#myModal2").html(alertHtml('error', xhr.responseText));
                    $('#myModal2').modal('show');

                }
            });
        }
    });
}


function detReg2(control, urlD) {



    $(control).live('click', function (e) {

        var strReg = "";

        $('#divGridView input:checked').each(function () {
            if (this.name != "chkSelectAll") {
                strReg = strReg + this.value + "|";
            }
        });

        if (strReg != "") {
            var strData = new Object();
            strData.codigo = strReg;
            $.ajax({
                type: 'POST',
                url: urlD,
                data: JSON.stringify(strData),
                beforeSend: function () {
                    $("#myModal").html('');
                },
                success: function (data) {
                    $("#myModal").html(data);

                    $('#myModal').on('shown', function () {
                        google.maps.event.trigger(map, "resize");
                    });

                    $('#myModal').modal('show');

                },
                error: function (xhr, status, error) {

                    $("#myModal").html(alertHtml('error', xhr.responseText));
                    $('#myModal').modal('show');

                }
            });
        } else {
            $("#myModal").html(alertHtml('erroCheck'));
            $('#myModal').modal('show');
        }
    });
}

function exportarReg(control, urlXLS, grilla) {
    $(control).live('click', function (e) {
        var filas = getNumRows(grilla);
        if (filas > 0) {
            window.location.href = urlXLS + '?' + getParametrosXLS();
        }
        else {
            $('#myModal').html(alertHtml('alertValidacion', "No existe data para exportar"));
            $('#myModal').modal('show');

        }
    });
}

function exportarRegDet(control, urlXLS, grilla, parameters) {
    $(control).live('click', function (e) {
        var filas = getNumRows(grilla);
        if (filas > 0) {
            window.location.href = urlXLS + '?' + parameters;
        }
        else {
            $('#myModal').html(alertHtml('alertValidacion', "No existe data para exportar"));
            $('#myModal').modal('show');
        }
    });
}

function getNumRows(grilla) {
    var numRows = "";
    numRows = $(grilla).find('tr').length;
    return numRows;
}

function cargacomboA(Padre, Hijo, pag) //funcion encargada de manejar combos anidados sin la necesidad de realizar algun postback
{
    console.log("cargacomboA");
    $(document).ready(function () {
        //elegido=$(Padre).val();
        elegido = Padre;

        $.post(pag, { elegido: elegido }, function (data) {
            $(Hijo).html(data);
        });
        console.log("a-" + Hijo);
        $(Hijo).change();
    });
}

/*CARGA*/
function accionesCarga() {


    $('.btnError').live('click', function (e) {
        e.preventDefault();

        tit = $(this).attr("idC");
        $("#myModal").html(alertHtml("errorCarga", $(this).parent().find('.errmsg').html(), tit));
        $('#myModal').modal('show');

    });


    $('#buscar').live("click", function (e) {
        e.preventDefault();
        $('.form-box').show();
        $('#divGridViewData').html('');
        $('.divUploadNew').hide();

    });


    var bex = -1;
    var bey = 0;
    var beerror = 0;

    $('.boton-ejecutar').live("click", function (e) {
        e.preventDefault();
        console.log(".boton-ejecutar");

        bex = -1;
        bey = 0;
        beerror = 0;

        $.ajax({
            type: "POST",
            url: "CargaDatos.aspx/ejecutarArchivoDTS",

            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            cache: false,
            beforeSend: function () {
                $("#myModal").html('<div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> ×</button> <h3 id="myModalLabel"> Cargando</h3> </div> <div id="myModalContent" class="modal-body"> <div class="cz-util-center-text"><p>Ejecutando...</br>Espere un momento por favor...</p></div> </div> <div class="modal-footer"> </div>');
                $('#myModal').modal('show');
            },
            success: function (msg) {
                $('#myModal').modal('hide');
                $('#file-uploader').find('.qq-upload-list li').remove();
                $('.boton-ejecutar').hide();
                $('#divGridViewData').html(msg.d);
                $('.form-box').hide();
                $('.divUploadNew').show();
                $("#myModal").html("");
                $("#divGridViewData table tr").each(function () {
                    bex++;
                    if (bex < $("#divGridViewData table tr").length) {
                        if (bex != 0) {
                            while (bey < $(this).find("td").length) {
                                console.log("fila y columna:" + bex + "-" + bey);
                                if (bey == 1) {
                                    var detalle_error = $(this).find(":nth-child(" + (bey + 1) + ") .errmsg").html();
                                    if (detalle_error.length > 0) {
                                        beerror++;
                                        $("#myModal").html($("#myModal").html() + "<br>" + $(this).find(":nth-child(" + (bey) + ")").html() + ": 1");
                                    }
                                }
                                bey++;
                            }
                            bey = 0;
                        }
                    }
                });

                //if (beerror > 0) {
                //    $("#myModal").html('<title> </title> <form method="post" action="repDet.aspx" id="form1"> </div> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> ×</button> <h3 id="myModalLabel"> Detalle</h3> </div> <div id="myModalContent" class="modal-body"> <div class="cz-util-center-text"><p>Se ah encontrado errores de carga en las filas: ' + $("#myModal").html() + '</p></div> </div> <div class="modal-footer"> </div> </form> ');
                //    $('#myModal').modal('show');
                //}
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml("error", jQuery.parseJSON(xhr.responseText).Message));
                $('#myModal').modal('show');
            }
        });
    });



    $('.boton-borrar').live("click", function (e) {
        e.preventDefault();
        var listFiles = $('#file-uploader').find('.qq-upload-list li');

        var idclass = $(this).attr("idclass");
        var filename = $(this).attr("filename");
        var error = $(this).attr("error");

        var name = filename.substring(0, filename.lastIndexOf('.'));
        var ext = filename.substr(filename.lastIndexOf('.') + 1);

        var jsonFileName = name;
        var jsonFileExt = ext;

        listFiles.find("#fri-" + idclass).remove();
        if (error == "si")
        { listFiles.find("#fbd-" + idclass).append("<div class='file-load-icon' id='fld-" + idclass + "'><img src='../images/icons/loader/ico_loader_red.gif' /></div>"); }
        else
        { listFiles.find("#fbd-" + idclass).append("<div class='file-load-icon' id='fld-" + idclass + "'><img src='../images/icons/loader/ico_loader_lightorange.gif' /></div>"); }

        var strData = new Object();
        strData.filename = jsonFileName;
        strData.fileext = jsonFileExt;

        $.ajax({
            type: "POST",
            url: "CargaDatos.aspx/borrarArchivo",
            data: JSON.stringify(strData),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            cache: false,
            success: function (msg) {
                $('.cz-form-step-examinar').show();
                listFiles.find("#fld-" + idclass).parent().parent().parent().parent().remove();
                if (listFiles.length > 1) {
                    $('.boton-ejecutar').show();
                }
                else {
                    $('.boton-ejecutar').hide();
                }
            },
            error: function (xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                listFiles.find("#fld-" + idclass).remove();
                listFiles.find("#fbd-" + idclass).addClass("file-red");
                listFiles.find("#fbd-" + idclass).append("<div class='file-result-icon'><img src='../images/icons/carga/ico_carga_error.png' /></div>");
                listFiles.find("#fbx-" + idclass).append("<div class='file-result-text file-red-font' id='frs-" + idclass + "'>" + err.Message + "</div>");
            }
        });
    });
}

function cargaSup(control) {
    var strperfil = $('#MddlPerfil option:selected').val();
    if (strperfil == 'V') {
        $('.filsup').show();
    }
    else {
        $('.filsup').hide();
    }
}

function cargaSup2() {
    var strperfil = $('#vddlPerfil option:selected').val();
    if (strperfil == 'V') {
        $('.filsup2').show();
    }
    else {
        $('.filsup2').hide();
    }
}
//personalizacion


function modRegicono() {

   /* $('.editItemRegicono').live("click", function (e) {*/
    $('#cz-form-box').on('click', '.editItemRegicono2', function (e) {
        //var strReg = $(this).attr('cod')
        /*var strReg = '6309'*/
        //var strData = new Object();
        //strData.codigo = strReg;
        /*alert(strData.codigo);*/

        //var parametros = obtenerParametros();
        var strData = new Object();
        strData.id = parametros.idapp;
        alert(strData.id);

        $.ajax({
            type: 'POST',
            url: urlvericon,
            data: JSON.stringify(strData),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            cache: false,
            success: function (data) {
                $("#myModal1").html(data);
                $('#myModal1').modal('show');

                $('#saveReg').click(function (e) {
                    e.preventDefault();

                    var validateItems = true;
                    $('.requerid').each(function () {
                        $(this).parent().find('span').remove();
                        if ($(this).val() == "") {
                            //    $(this).after("<span style='color:#b94a48'>*</span>");
                            $(this).parent().find('.cz-form-content-input-text-visible').after("<span style='color:#b94a48'>" + $('#hObligatorio').val() + "</span>");
                            validateItems = false;
                        }
                    });
                    if (validateItems) {
                        $('#divError').attr('class', "alert fade");

                        var hModal = $("#myModal1").height();

                        $.ajax({
                            type: 'POST',
                            url: urlvericon,
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            async: true,
                            cache: false,
                            data: JSON.stringify(getData()),
                            success: function (data) {

                                addnotify("notify", data.d, "registeruser");

                                $('#myModal1').modal('hide');
                                $('#buscar').trigger("click");

                            },
                            error: function (xhr, status, error) {
                                addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "registeruser");


                            }
                        });

                    }
                });

            },
            error: function (xhr, status, error) {

                $("#myModal1").html(alertHtml('error', xhr.responseText));
                $('#myModal1').modal('show');

            }
        });

    });
}



/*Agrega Registros*/// esta es la funcion 
function nuevoCliente() {
    $('.addCliente').click(function (e) {
        $.ajax({
            type: 'POST',
            url: urlNew,
            success: function (data) {
                $("#myModal").html(data);
                $('#myModal').modal('show');

                $('#saveReg').click(function (e) {
                    e.preventDefault();

                    var validateItems = true;

                    $('.requerid').each(function () {
                        $(this).parent().find('span').remove();
                        if ($(this).val() == "") {

                            $(this).parent().find('.cz-form-content-input-text-visible').after("<span style='color:#b94a48'>" + $('#hObligatorio').val() + "</span>");
                            $(this).parent().find('.cz-form-content-input-select-visible').after("<span style='color:#b94a48'>" + $('#hObligatorio').val() + "</span>")

                            validateItems = false;
                        }
                    });

                    if (validateItems) {

                        $('#divError').attr('class', "alert fade");
                        var hModal = $("#myModal").height();
                        $.ajax({
                            type: 'POST',
                            url: urlsavN,
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            async: true,
                            cache: false,
                            data: JSON.stringify(getData()),
                            success: function (data) {
                                addnotify("notify", data.d, "registeruser");
                                clearCampos();
                                $('#myModal').modal('hide');
                                $('#buscar').trigger("click");
                            },
                            error: function (xhr, status, error) {
                                addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "registeruser");
                            }
                        });

                    }
                });

            },
            error: function (xhr, status, error) {

                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');

            }
        });

    });

}

//function nuevoUsuario() {
//    $('.addUsuario').click(function (e) {
//        $.ajax({
//            type: 'POST',
//            url: urlNewusu,
//            success: function (data) {
//                $("#myModal").html(data);
//                $('#myModal').modal('show');

//                $('#btnGuardarUsuario').click(function (e) {
//                    e.preventDefault();

//                    var validateItems = true;

//                    $('.requerid').each(function () {
//                        $(this).parent().find('span').remove();
//                        if ($(this).val() == "") {

//                            $(this).parent().find('.cz-form-content-input-text-visible').after("<span style='color:#b94a48'>" + $('#hObligatorio').val() + "</span>");
//                            $(this).parent().find('.cz-form-content-input-select-visible').after("<span style='color:#b94a48'>" + $('#hObligatorio').val() + "</span>")

//                            validateItems = false;
//                        }
//                    });

//                    if (validateItems) {

//                        $('#divError').attr('class', "alert fade");
//                        var hModal = $("#myModal").height();
//                        alert("ALERT ANTES DEL AJAX");
//                        $.ajax({
//                            type: 'POST',
//                            url: urlinsusu,
//                            contentType: "application/json; charset=utf-8",
//                            dataType: "json",
//                            async: true,
//                            cache: false,
//                            data: JSON.stringify(getNuevoUsuario()),
//                            beforeSend: function () {
//                                alert(JSON.stringify(getNuevoUsuario()));
//                            },
//                            success: function (data) {
//                                addnotify("notify", data.d, "registeruser");
//                                clearCampos();
//                                $('#myModal').modal('hide');
//                                $('#buscar').trigger("click");
//                            },
//                            error: function (xhr, status, error) {
//                                addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "registeruser");
//                            }
//                        });

//                    }
//                });

//            },
//            error: function (xhr, status, error) {
//                alert("ERROR 1");
//                $("#myModal").html(alertHtml('error', xhr.responseText));
//                $('#myModal').modal('show');

//            }
//        });

//    });

//}

// Guardar nuevo cliente
function agregarCliente() {
    $('.addCliente').click(function (e) {
        $.ajax({
            type: 'POST',
            url: urlNew,
            success: function (data) {
                $("#divContenidoModal").html(data);
                $('#divMyModal').modal({ backdrop: 'static', keyboard: false, show: true });

                $('#btnGuardarCliente').click(function (e) {
                    e.preventDefault();
                    var validateItems = true;

                    $('#txtNewRuc').attr('style', '');
                    $('#txtNewRazonSocial').attr('style', '');
                    $('#txtNewNombreComercial').attr('style', '');

                    if ($('#txtNewRuc').val() == '') {
                        $('#txtNewRuc').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese el RUC de la empresa.", "registeruser");
                        validateItems = false;
                    } else if ($('#txtNewRazonSocial').val() == '') {
                        $('#txtNewRazonSocial').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese la Razón Social.", "registeruser");
                        validateItems = false;
                    }
                    else if ($('#txtNewNombreComercial').val() == '') {
                        $('#txtNewNombreComercial').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese el Nombre Comercial.", "registeruser");
                        validateItems = false;
                    }
                    if (validateItems) {
                        $.ajax({
                            type: 'POST',
                            url: urlins,
                            contentType: "application/json; charset=utf-8",
                            async: true,
                            cache: false,
                            data: JSON.stringify(getNuevoCliente()),
                            
                            success: function (data) {
                                addnotify("notify", "Cliente agregado correctamente.", "registeruser");
                                $('#divContenidoModal').html(data);
                                $('#myModal').modal('hide');
                                buscarCliente();
                                $('#buscar').trigger('click');
                            },
                            error: function (xhr, status, error) {
                                $('#msg').html('');
                                //$('#btnLimpiar').show();
                                $('#btnGuardaTrabajoInc').show();
                                $('#btnCancelar').show();
                                addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                            }
                        });
                    }
                });

            },
            error: function (xhr, status, error) {
                $("#divContenidoModal").html(alertHtml('error', xhr.responseText));
                $('#divMyModal').modal('hide');

            }
        });

    });
}


// Agregar usuario
function agregarUsuario() {
    $('.addUsuario').click(function (e) {
        $.ajax({
            type: 'POST',
            url: urlNewusu,
            success: function (data) {
                $("#myModal").html(data);
                $('#myModal').modal('show');

                $(document).on("click", "#btnGuardarUsuario", function (e) {
                    e.preventDefault();
                    var validateItems = true;

                    $('#txtPerfilVer').attr('style', '');
                    $('#txtNewUsuario').attr('style', '');
                    $('#txtNewLogin').attr('style', '');
                    $('#txtNewPassword').attr('style', '');

                    if ($('#txtPerfilVer').val() == '-1') {
                        $('#txtPerfilVer').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Seleccione un tipo de perfil.", "registeruser");
                        validateItems = false;
                    } else if ($('#txtNewUsuario').val() == '') {
                        $('#txtNewUsuario').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese el nombre del usuario.", "registeruser");
                        validateItems = false;
                    }
                    else if ($('#txtNewLogin').val() == '') {
                        $('#txtNewLogin').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese el login.", "registeruser");
                        validateItems = false;
                    } else if ($('#txtNewPassword').val() == '') {
                        $('#txtNewPassword').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese un password.", "registeruser");
                        validateItems = false;
                    }
                    if (validateItems) {

                        //$('#divError').attr('class', "alert fade");
                        //var hModal = $("#myModal").height();
                        $.ajax({
                            type: 'POST',
                            url: urlinsusu,
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            async: true,
                            cache: false,
                            data: JSON.stringify(getNuevoUsuario()),
                            success: function (data) {
                                addnotify("notify", "Usuario registrado", "registeruser");
                                $('#divContenidoModal').html(data);
                                $('#myModal').modal('hide');
                                buscarUsuario();
                                $('#buscar').trigger("click");
                            },
                            error: function (xhr, status, error) {
                                addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "registeruser");
                            }
                        });
                    }
                });

            },
            error: function (xhr, status, error) {
                $("#divContenidoModal").html(alertHtml('error', xhr.responseText));
                $('#divMyModal').modal('hide');

            }
        });

    });
}


// Editar cliente
function editarCliente() {
    $(document).on("click", ".btnEditarCliente", function (e) {
        var strReg = $(this).attr('idcli');
        var strDatas = new Object();
        strDatas.idCliente = strReg;
        //alert(strReg);

        $.ajax({
            type: 'POST',
            url: urlgetCliente,
            async: true,
            cache: false,
            data: JSON.stringify(strDatas),
            success: function (data) {
                $('#myModal').html(data);
                $('#myModal').modal('show');

                $('#btnEditarCliente').click(function (e) {

                    var validarItems = true;
                    $('#txtEditRuc').attr('style', '');
                    $('#txtEditRazonSocial').attr('style', '');
                    $('#txtEditNombreComercial').attr('style', '');

                    if ($('#txtEditRuc').val() == '') {
                        $('#txtEditRuc').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese el RUC de la empresa.", "registeruser");
                        validarItems = false;
                        } else
                    if ($('#txtEditRazonSocial').val() == '') {
                        $('#txtEditRazonSocial').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese la Razón Social.", "registeruser");
                        validarItems = false;
                        } else
                    if ($('#txtEditNombreComercial').val() == '') {
                        $('#txtEditNombreComercial').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese el Nombre Comercial.", "registeruser");
                        validarItems = false;
                    }


                    if (validarItems) {
                        //alert(JSON.stringify(getEditarCliente()));
                        ////alert(urleditCliente);
                        ////var formData = { valor: "bart" };
                        //alert(JSON.stringify(formData));
                        $.ajax({
                            type: 'POST',
                            url: urleditCliente,
                            contentType: "application/json; charset=utf-8",
                            async: true,
                            cache: false,
                            data: JSON.stringify(getEditarCliente()),
                            beforeSend: function () {
                                //alert(getEditarCliente());
                                //$('#msg').html('<center><img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />Espere un momento por Favor...</center>');
                                //$('#btnEditarCliente').hide();
                                //$('#btnCancelar').hide();
                            },
                            success: function (data) {
                                addnotify("notify", "Cliente Actualizado", "registeruser");
                                $('#divContenidoModal').html(data);
                                $('#myModal').modal('hide');
                                buscarCliente();
                                $('#buscar').trigger('click');
                            },
                            error: function (xhr, status, error) {
                                //alert(error);
                                $('#msg').html('');
                                $('##btnEditarCliente').show();
                                $('#btnCancelar').show();
                                addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                            }
                        });
                    }
                }
                );

            },
            error: function (xhr, status, error) {
                alert('xhr: ' + JSON.stringify(xhr) + ' status: ' + JSON.stringify(status) + ' error: ' + JSON.stringify(error));
                //$("#myModal").html(alertHtml('error', xhr.responseText));
                //$('#myModal').modal('show');

            }
        });

    });

}

// Editar usuario
function editUsuario() {
    $(document).on("click", ".btnEditarUsuario", function (e) {
        var strReg = $(this).attr('idusu');
        var strDatas = new Object();
        strDatas.idUsuario = strReg;
        //alert(strReg);

        $.ajax({
            type: 'POST',
            url: urlgetUsuario,
            async: true,
            cache: false,
            data: JSON.stringify(strDatas),
            success: function (data) {
                $('#myModal').html(data);
                $('#myModal').modal('show');

                $('#btnActUsuario').click(function (e) {

                    var validarItems = true;
                    $('#txtEditPerfil').attr('style', '');
                    $('#txtEditUsuario').attr('style', '');
                    $('#txtEditLogin').attr('style', '');
                    $('#txtEditPassword').attr('style', '');

                    if ($('#txtEditPerfil').val() == '') {
                        $('#txtEditPerfil').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Seleccione un perfil.", "registeruser");
                        validarItems = false;
                    } else
                    if ($('#txtEditUsuario').val() == '') {
                        $('#txtEditUsuario').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese el nombre del usuario.", "registeruser");
                        validarItems = false;
                    } else
                    if ($('#txtEditLogin').val() == '') {
                        $('#txtEditLogin').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese el login del usuario.", "registeruser");
                        validarItems = false;
                    } else
                    if ($('#txtEditPassword').val() == '') {
                        $('#txtEditPassword').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese un password.", "registeruser");
                        validarItems = false;
                    }


                    if (validarItems) {
                        var strData = new Object();
                        strData = getEditarUsuario();
                        $.ajax({
                            type: 'POST',
                            url: urleditUsuario,
                            contentType: "application/json; charset=utf-8",
                            async: true,
                            cache: false,
                            data: JSON.stringify(strData),
                            success: function (data) {
                                addnotify("notify", "Usuario Actualizado", "registeruser");
                                $('#divContenidoModal').html(data);
                                $('#myModal').modal('hide');
                                buscarUsuario();
                                $('#buscar').trigger("click");
                            },
                            error: function (xhr, status, error) {
                                
                                addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                            }
                        });
                    }
                }
                );

            },
            error: function (xhr, status, error) {
                alert('xhr: ' + JSON.stringify(xhr) + ' status: ' + JSON.stringify(status) + ' error: ' + JSON.stringify(error));
                //$("#myModal").html(alertHtml('error', xhr.responseText));
                //$('#myModal').modal('show');

            }
        });

    });

}


function estadoCombo() {
    if ($('.selcombo').val() == 1) {
        localStorage.setItem("chbactivo", 1);
    } else {
        localStorage.setItem("chbactivo", 2);
    }

    //if ($('#check').is(':checked')) {
    //    localStorage.setItem("chbactivo", 1);
    //} else {
    //    localStorage.setItem("chbactivo", 2);
    //}

}


//SIEDEVS - 12-05-2016 - JHONATAN MORENO

function buscarPerfiles() {
    $('#buscarPerfiles').click(function (e) {
        //cargando la grilla
        var strData = getParametros();
        strData.pagina = 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $('.form-gridview-data').html('');
                $('.form-gridview-error').html('');
                $('.form-gridview-search').show();
            },
            success: function (data) {
                //console.log('data_: ' + data);
                $('.form-gridview-search').hide();
                $('.form-gridview-error').html('');
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
            },
            error: function (xhr, status, error) {
                $('.form-gridview-search').hide();
                $('.form-gridview-data').html();
                $('.form-gridview-error').html("<p><strong>ERROR: </strong> Se ha producido un error en el codigo. Para mayor detalle </p><a id='divError'>haga click aqui</a>");
                $("#myModal").html(alertHtml('error', xhr.responseText));

                $('#divError').click(function (e) {
                    $('#myModal').modal('show');
                });
            }
        });

    });


    $('.pagina-anterior').live('click', function (e) {

        var strData = getParametros();
        strData.pagina = parseInt(strData.pagina) - 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });

    $('.pagina-siguiente').live('click', function (e) {

        var strData = getParametros();
        strData.pagina = parseInt(strData.pagina) + 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });
}

//SIEDEVS - 12-05-2016 - JHONATAN MORENO

function buscarModulos() {
    $('#buscarPerfiles').click(function (e) {
        //cargando la grilla
        var strData = getParametrosModulos();
        strData.pagina = 1;
        strData.idPerfil = "TODO";
        $.ajax({
            type: 'POST',
            url: urlbusModulo,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $('.form-gridview-data-new').html('');
                $('.form-gridview-error-new').html('');
                $('.form-gridview-search-new').show();
            },
            success: function (data) {
                //console.log('data_: ' + data);
                $('.form-gridview-search-new').hide();
                $('.form-gridview-error-new').html('');
                $('.form-gridview-data-new').html(data);
                $('#hdnActualPageModulo').val(strData.pagina);
            },
            error: function (xhr, status, error) {
                $('.form-gridview-search-new').hide();
                $('.form-gridview-data-new').html();
                $('.form-gridview-error-new').html("<p><strong>ERROR: </strong> Se ha producido un error en el codigo. Para mayor detalle </p><a id='divError'>haga click aqui</a>");
                $("#myModal").html(alertHtml('error', xhr.responseText));

                $('#divError').click(function (e) {
                    $('#myModal').modal('show');
                });
            }
        });

    });


    $('.pagina-anterior-new').live('click', function (e) {

        var strData = getParametrosModulos();
        strData.pagina = parseInt(strData.pagina) - 1;
        $.ajax({
            type: 'POST',
            url: urlbusModulo,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data-new").hide();
                $(".paginator-data-search-new").show();
            },
            success: function (data) {
                $('.form-gridview-data-new').html(data);
                $('#hdnActualPageModulo').val(strData.pagina);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer-new").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });

    $('.pagina-siguiente-new').live('click', function (e) {

        var strData = getParametrosModulos();
        strData.pagina = parseInt(strData.pagina) + 1;
        $.ajax({
            type: 'POST',
            url: urlbusModulo,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data-new").hide();
                $(".paginator-data-search-new").show();
            },
            success: function (data) {
                $('.form-gridview-data-new').html(data);
                $('#hdnActualPageModulo').val(strData.pagina);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer-new").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });
}

function buscarModulosPorPerfil() {
    var perfil;
    $('.cargaModulo').live('click', function (e) {
        $('#divGridView input:radio:checked').each(function (e) {
            perfil = $(this).attr('idPerfil');
            //alert(strData.idPerfil);
        });
        //cargando la grilla
        var strData = getParametrosModulos();
        strData.idPerfil = perfil;
        strData.pagina = 1;
        $.ajax({
            type: 'POST',
            url: urlbusModulo,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $('.form-gridview-data-new').html('');
                $('.form-gridview-error-new').html('');
                $('.form-gridview-search-new').show();
            },
            success: function (data) {
                //console.log('data_: ' + data);
                $('.form-gridview-search-new').hide();
                $('.form-gridview-error-new').html('');
                $('.form-gridview-data-new').html(data);
                $('#hdnActualPageModulo').val(strData.pagina);
            },
            error: function (xhr, status, error) {
                $('.form-gridview-search-new').hide();
                $('.form-gridview-data-new').html();
                $('.form-gridview-error-new').html("<p><strong>ERROR: </strong> Se ha producido un error en el codigo. Para mayor detalle </p><a id='divError'>haga click aqui</a>");
                $("#myModal").html(alertHtml('error', xhr.responseText));

                $('#divError').click(function (e) {
                    $('#myModal').modal('show');
                });
            }
        });

    });


    $('.pagina-anterior-new').live('click', function (e) {

        var strData = getParametrosModulos();
        strData.pagina = parseInt(strData.pagina) - 1;
        $.ajax({
            type: 'POST',
            url: urlbusModulo,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data-new").hide();
                $(".paginator-data-search-new").show();
            },
            success: function (data) {
                $('.form-gridview-data-new').html(data);
                $('#hdnActualPageModulo').val(strData.pagina);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer-new").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });

    $('.pagina-siguiente-new').live('click', function (e) {

        var strData = getParametrosModulos();
        strData.pagina = parseInt(strData.pagina) + 1;
        $.ajax({
            type: 'POST',
            url: urlbusModulo,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data-new").hide();
                $(".paginator-data-search-new").show();
            },
            success: function (data) {
                $('.form-gridview-data-new').html(data);
                $('#hdnActualPageModulo').val(strData.pagina);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer-new").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });
}

function ObtenerConsultoresRequerimientos() {
    $("#ddlZona").change(function () {
        var strData = new Object();
        strData.idZona = $("#ddlZona").val();
        strData.idUsuario = $("#hdnIdUsuario").val();
        strData.idPerfil = $("#hdnIdPerfil").val();
        $.ajax({
            type: 'POST',
            url: urlGetConsultores,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            success: function (msg ) {
                $("#ddlConsultores").html(null);
                var datos = $.parseJSON(msg.d);
                //ddlConsultores
                var option = $(document.createElement('option'));
                option.text(".:SELECCIONE:.");
                option.val("-1"); 
                $("#ddlConsultores").append(option);
                $(datos).each(function () {
                    var option = $(document.createElement('option'));

                    option.text(this.nombre);
                    option.val(this.id);
                    $("#ddlConsultores").append(option);

                });
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });
    });
}