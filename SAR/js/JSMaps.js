﻿var r=0;
var offset = { 'dashed': 0, 'dotted': 0 };
var markers = new Array();
var routes;
var polyline;
var infowindow= new Array();
var map;

function initialize(lat,lng)
 {

    var latlng = new google.maps.LatLng(lat,lng);
    var myOptions = {
        navigationControl: true,
        mapTypeControl: true,
        mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
        scaleControl: true,
        zoom: 12,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };


    map = new google.maps.Map(document.getElementById("map"), myOptions);

    routes = new google.maps.MVCArray();
    polyline = new google.maps.Polyline({

    map: map
    , strokeColor: '#ff0000'
    , strokeWeight: 3
    , strokeOpacity: 0.2
    , clickable: false
    , icons: [{ icon: { path: 'M 0,-2 0,2', strokeColor: 'ff0000', strokeOpacity: 0.6, }, repeat: '25px' }]
    ,path: routes
    });
    
    google.maps.event.addListener(map, 'click', function(){
        for (var i = 0; i < infowindow.length-1; i++) {
            infowindow[i].close();
        }
    });
}

  
function cargaPoints(urlM,polilyne)
{ 
   deleteMarker(); 
   
   $.ajax({
            type: 'POST',
            url: urlM,
            data:JSON.stringify(getData()),
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            dataType: "json",
           
            success: function (data) {
              
              
              $.each(jQuery.parseJSON(data.d), function(index, objPoint) {
                crearPoint(objPoint,jQuery.parseJSON(data.d));
                
              });
              setTimeout(function(){
              map.setCenter(setZoom());},1000);


            },
            
            error: function (xhr, status, error) {
              
            }
        }); 
        
     
}

function cargaPointsUlt(urlM,polilyne)
{ 
   deleteMarker(); 
   
   $.ajax({
            type: 'POST',
            url: urlM,
            data:JSON.stringify(getData()),
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            dataType: "json",
            beforeSend: function () {

                $(".form-gridview-search").show();
            },
            success: function (data) {
              
               $(".form-gridview-search").hide();
               if(jQuery.parseJSON(data.d).length>0)
               {
                  $.each(jQuery.parseJSON(data.d), function(index, objPoint) {
                  if(objPoint.latitud!="0.0")
                  {
                    crearPoint(objPoint,jQuery.parseJSON(data.d));
                  }
                
                  });
                  setTimeout(function(){
                  map.setCenter(setZoom());},1000);

              }
              else
               {
                   addnotify("notify", "No existen puntos", "divError");
              }

            },
            
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        }); 
        
     
}

  
  
function crearPoint(objPoint,Data)
{
    addMarker(objPoint.latitud,objPoint.longitud,objPoint.msg,objPoint.img,Data);
}

function deleteMarker() {

if (markersArray.length>0) {
    for (var i= 0;i < markersArray.length; i++) {

         markersArray[i].setMap(null);
    }
    
    routes = new google.maps.MVCArray();
    polyline.setPath(routes);
    markersArray.length = 0;
    infowindow= new Array();
    
    }
}


function addMarker(lat,lng,info,image,obj) {


    var repetidos = new Array(); 
    var newLocation = new google.maps.LatLng(lat, lng);
    var marker = new google.maps.Marker({
        icon:image,
        position: newLocation,
        animation: google.maps.Animation.DROP,
        map: map});

    markersArray.push(marker);

    markers[r++]=marker;
    map.setCenter(newLocation);
    var text = info ;
    var infowindow = new google.maps.InfoWindow({ content: text });
    repetidos = buscarRepetidos(lat,lng,obj)

    if(repetidos.length > 1 ){        
            var inf = infowindowsRep(repetidos, obj);        
            infowindow = new google.maps.InfoWindow({ content: inf });   
    }
        
    google.maps.event.addListener(marker, 'click', function() {
        for (var i = 0; i < infowindow.length-1; i++) {
            infowindow[i].close();
        }
        infowindow.open(map,marker);
    });
    
}


function addPolilyne(lat,lng) {
    var newLocation = new google.maps.LatLng(lat, lng);
    map.setCenter(newLocation);
    routes.push(newLocation);
    polyline.setPath(routes);
    polylineTimer = setInterval(function () { animateDashed(); }, 200);

}

function buscarRepetidos(lat,lon, data){
    var arregloPuntosRepetidos = new Array(); 
    var j = 0;  
    $.each(data, function(index, objPoint) {
        if(lat==objPoint.latitud && lon== objPoint.longitud)
        {
         arregloPuntosRepetidos[j++] = index;
        }
    });
    
    
    return arregloPuntosRepetidos; 
}

function infowindowsRep(arrRep, data)
{

    var infow = ""; 
    setdataIw(data);
    infow += "<table  cellpadding='0'height='100%' align='center'><thead><tr><th>Puntos</th></tr></thead><tbody>";
    for(i=0; i < arrRep.length; i++)
    {
        var sec = arrRep[i];
         infow += "<tr><td>";
         infow += "<a href='javascript:markerRept("+  sec  +");'>"+ data[sec].titulo +"</a></td></tr>"
    }

    infow += "</tbody></table>";     
    return infow;
}



function markerRept(sec){
    var dato = new Array();
    dato = getDataIw();
    var mnsg = dato[sec].msg;   
    var lalo = new google.maps.LatLng(dato[sec].latitud, dato[sec].longitud);    
    var infowindow;  
    infowindow = new google.maps.InfoWindow({ content: mnsg  });
    infowindow.open(map,markers[sec]);
       
}

	

function setZoom() {
    
   //var boundbox = new google.maps.LatLngBounds();
    
   for ( var i = 0; i < markersArray.length; i++ ){
        map.setCenter(new google.maps.LatLng(markersArray[i].position.lat(), markersArray[i].position.lng()));
        //boundbox.extend(new google.maps.LatLng(markersArray[i].position.lat(),markersArray[i].position.lng()));
        }
   // map.fitBounds(boundbox);
   //return boundbox.getCenter();
 }




 
var datosIW;
function setdataIw(obj){
    datosIW =  obj;
    
}
function getDataIw(){
return datosIW;
} 



function animateDashed() {
    if (offset['dashed'] > 23) {
      offset['dashed'] = 0;
    } else {
      offset['dashed'] += 2;
    }
    var icons = polyline.get('icons');
    icons[0].offset = offset['dashed'] + 'px';
    polyline.set('icons', icons); 
    
    
}