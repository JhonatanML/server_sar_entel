﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Menu.aspx.cs" Inherits="SAR.Menu" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title><%=ConfigurationManager.AppSettings["APP_NAME"] %> | Entel del Peru S.A.</title>
    <link rel="shortcut icon" href="images/icons/monitoreoBD.ico" />
    <script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <%--<script src="js/bootstrap.min.js" type="text/javascript"></script>--%>
    <link href="https://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet" />
    <%--<link href="https://getbootstrap.com/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet" />
    <link href="https://getbootstrap.com/examples/navbar-fixed-top/navbar-fixed-top.css" rel="stylesheet" />
    <script src="https://getbootstrap.com/assets/js/ie-emulation-modes-warning.js" type="javascript"></script>--%>
    <script src="https://getbootstrap.com/dist/js/bootstrap.min.js" type="javascript"></script>
    <%--<script src="https://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js" type="javascript"></script>--%>

    <link href="css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="js/cz_main.js" type="text/javascript"></script>
    <%--<link href="css/Siedevs.css" rel="stylesheet" />--%>

    <style type="text/css">
        #idPerfilEsconder {
            display: none;
        }

        .nav > li > a:focus, .nav > li > a:hover {
            background-color: #214E8C;
            color: #fff;
        }

        .navMenu {
            color: #f8f8f8;
            font-size: 16px;
        }

        .navbar-nav > li > a {
            height: 60px;
        }

       
    </style>

</head>
<body>
    <form id="form1" runat="server" class="cz-main">
        <div class="cz-submain">
            <div id="cz-box-header2">
                <div id="cz-barr-top">
                    <div id="cz-control-top" class="cz-box-center-width">
                        <div id="img_logo_top"></div>
                        <ul class="nav navbar-nav" style="height: 60px;">
<%--                            <li class="active"><a class="navMenu" href="#">Inicio</a></li>
                            <li><a class="navMenu" href="#">Acerca de</a></li>
                            <li><a class="navMenu" href="#">Blog</a></li>
                            <li><a class="navMenu" href="#">Contacto</a></li>--%>

                            <asp:Literal ID="MenuTop" runat="server"></asp:Literal>
                        </ul>
                        <div id="cz-control-options">
                            <div id="cz-control-user">
                                <div id="cz-control-user-avatar" class="cz-control-user-option"></div>
                                <div class="cz-control-user-option">
                                    <asp:Label ID="lbNomUsuario" runat="server" Text=""></asp:Label>
                                </div>
                                <div id="cz-control-menu" class="cz-control-user-option">
                                    <div id="cz-control-menu-arrow"></div>
                                    <div id="cz-control-menu-options">
                                        <div class="cz-control-menu-option"><a href="inicio.aspx">Inicio</a></div>
                                        <div class="cz-control-menu-option parent">
                                            <asp:LinkButton ID="opcSalir" runat="server" OnClick="opcSalir_Click">Salir</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="cz-box-body2">
                <div id="cz-menu-lateral-left" class="cz-menu-lateral cz-menu-lateral-left cz-menu-lateral-hide">
                    <div id="cz-menu-lateral-options">
                        <asp:Literal ID="MenuLateral" runat="server"></asp:Literal>
                    </div>
                </div>

                <div id="cz-util-hidden-left" class="cz-util-hidden cz-util-hidden-width cz-util-hidden-right">
                    <div class="cz-util-hidden-arrow"></div>
                </div>

                <div id="cz-box-content">
                    <iframe name="centerFrame" id="centerFrame" width="100%" height="100%" scrolling="yes" frameborder="0"></iframe>
                </div>
            </div>
            <div id="cz-box-footer2">
                <p>SOLUCIONES DE DATOS.</p>
            </div>
        </div>
        <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>
        <div id="cz-background"></div>
        <div id="cz-alert">
            <div id="cz-alert-header">
                <div id="cz-alert-header-title">Mensaje</div>
                <div id="cz-alert-close">×</div>
            </div>
            <div id="cz-alert-content">
                Mensaje de prueba.
            </div>
        </div>
    </form>
</body>
</html>
