﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DetalleBitacora.aspx.cs" Inherits="SAR.Reportes.Bitacora.DetalleBitacora" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Envio APP</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--CSS-->
    <link href="../../css/normalize.css" rel="stylesheet" />
    <link href="../../css/bootstrap-toggle.min.css" rel="stylesheet" />
    <link href="../../css/bootstrap-tags.css" rel="stylesheet" />
    <link href="../../css/FormsEntel.css" rel="stylesheet" />
    <link href="../../css/BootStrap/bootstrap.min.css" rel="stylesheet" />
    <!--JS-->
    <script src="../../js/jquery-1.10.2.min.js"></script>
    <script src="../../js/cz_main.js" type="text/javascript"></script>
    <script src="../../js/jsmodule.js" type="text/javascript"></script>
    <script src="../../js/Validacion.js" type="text/javascript"></script>
    <script src="../../js/CheckedListGroup.js"></script>
    <script src="../../js/bootstrap-tags.min.js"></script>
    <script src="../../js/bootstrap-toggle.min.js"></script>
    <script src="../../js/BootStrap/bootstrap.min.js"></script>
    <script src="../../js/ValidarCampos/ValidarCampos.js"></script>
    <style>
        .glyphicon {
            color: #FF6702;
            margin-right: 10px;
        }

        .list-group-item.active {
            background-color: #0154A0;
        }

        .panel-heading {
            height: 45px !important;
        }

        a:focus, a:hover {
            text-decoration: none;
            color: #FF6708 !important;
        }

        p {
            margin-top: 6px;
            margin-bottom: 0px;
        }
    </style>
    <script>
        var urlvericon = 'HistorialCambios.aspx';
        var urlUpdate = "DetalleApp.aspx/ActualizarDatosApp";

        $(document).ready(function () {

            MantDetalleApp();

            $('#tVersionSuiteX').validCampoFranz('0123456789.');
            $('#tVersionesWeb').validCampoFranz('0123456789.');
            $('#txtVersionSerMovil').validCampoFranz('0123456789.');
            $('#txtVersionMovil').validCampoFranz('0123456789.');

            $('#cz-form-box-atras').click(function () {
                parent.history.back();
                return false;
            });

            $('#ddlHosting').change(function () {
                var id = $('#ddlHosting').val();
                $('#ddlHosting option[value=' + id + ']').attr('selected', 'selected');
            });

            $('.solo-numero').keyup(function () {
                this.value = (this.value + '').replace(/[^0-9,]/g, '');
            });

            $('.editItemRegicono2').click(function () {
                var validateItems = true;

                $('.requerid').each(function () {
                    $(this).attr('style', '');
                    if ($(this).val() == "") {
                        $(this).attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        validateItems = false;
                    }
                });
                var strData = new Object();
                strData.idApp = $('#hdIdApp').val();
                $.ajax({
                    type: 'POST',
                    url: 'MuestraHistorial.aspx',
                    contentType: "application/json; charset=utf-8",
                    async: true,
                    cache: false,
                    data: JSON.stringify(strData),
                    beforeSend: function () {
                        //$('#divResultado').html('<center><span class="glyphicon glyphicon-hourglass" style="color:#FF6702;" aria-hidden="true"></span><span class="sr-only">Load:</span> Espere un Momento Por Favor</center>');
                        $("#divContenidoModal").html(alertHtml('cargando', '', ''));
                        $('#divMyModal').modal({ backdrop: 'static', keyboard: false, show: true });
                    },
                    success: function (data) {
                        $("#divContenidoModal").html(data);
                    },
                    error: function (xhr, status, error) {
                        $("#divContenidoModal").html(alertHtml('error', xhr.responseText));

                        $('#divError').click(function (e) {
                            $('#divMyModal').modal('show');
                        });
                    }
                });
            });

            $('#divSelecc').click(function () {
                if ($('#divSelecc').attr('selec') == '1') {
                    $('#divSelecc').attr('selec', '0');
                    $('#textSelec').text('Deseleccionar Todo')
                    $("#checkListNumeros li").each(function (idx, li) {
                        $(li).addClass('active');
                        $('.glyphicon-unchecked').removeClass('glyphicon-unchecked').addClass('glyphicon-check');
                    });
                } else {
                    $('#divSelecc').attr('selec', '1');
                    $('#textSelec').text('Seleccionar Todo')
                    $("#checkListNumeros li").each(function (idx, li) {
                        $(li).removeClass('active');
                        $('.glyphicon-check').removeClass('glyphicon-check').addClass('glyphicon-unchecked');
                    });
                }

            });

        });

        function obtenerDetalleApp() {
            var strData = new Object();
            strData.vSuite = $('#tVersionSuiteX').val();
            strData.vWeb = $('#tVersionesWeb').val();
            strData.vServerMovil = $('#txtVersionSerMovil').val();
            strData.vMovil = $('#txtVersionMovil').val();
            strData.urlApk = $('#txtUrlApk').val();
            strData.urlWeb = $('#txtUrlWeb').val();
            strData.urlMobile = $('#txtUrlMobile').val();
            strData.nombreBD = $('#txtNombreBD').val();
            strData.detalle = $('#tDetalle').val();
            strData.estado = $('#ddlEstado').val();
            strData.hosting = $('#ddlHosting').val();
            strData.idOperador = $('#hdnIdOperador').val();
            strData.idApp = $('#hidapp').val();
            strData.idCliente = $('#hdnIdCliente').val();
            strData.idPlataforma = $('#hdnIdPlataforma').val();
            strData.idModulo = $('#hdnIdModulo').val();
            strData.descApp = $('#hdnDescApp').val();
            strData.login = $('#hdnLogin').val();
            return strData;
        }



    </script>
    <style type="text/css">
        .urlColor {
            color: #0154a0;
        }
    </style>
</head>
<body>

    <form id="form1" runat="server">
        <div id="divMyModal" class="modal fade">
            <div class="modal-dialog" id="divContenidoModal"></div>
        </div>
        <a href="#" id="back-to-top" title="Back to top">&uarr;</a>
        <div class="cz-submain cz-submain-form-background">
            <div id="cz-form-box">
                <div class="container-fluid cz-form-box-content">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <span class="glyphicon glyphicon-list-alt btn-lg" style="color: #666;" aria-hidden="true"></span>
                        <span id="spTituloVentana" style="font-weight: bold; font-size: 20px; color: #666;" runat="server">Información del Módulo
                        </span>
                    </div>

                    <button id="cz-form-box-grabar" type="button" class="cz-form-content-input-button btn-lg cz-form-box-content-button cz-util-right" style="margin-top: 5px;">
                        <span class="glyphicon glyphicon-floppy-saved" style="color: white;"></span>&nbsp;&nbsp;Grabar
                    </button>
                    <button id="cz-form-box-limpiar" type="button" class="cz-form-content-input-button btn-lg cz-form-box-content-button cz-util-right" style="margin-top: 5px;" data-toggle="modal" data-target="#myModal">
                        <span class="glyphicon glyphicon-erase" style="color: white;"></span>&nbsp;&nbsp;Limpiar
                    </button>

                    <button id="cz-form-box-atras" type="button" class="cz-form-content-input-button btn-lg cz-form-box-content-button cz-util-right xlsReg selectData" style="margin-top: 5px;">
                        <span class="glyphicon glyphicon-circle-arrow-left" style="color: white;"></span>&nbsp;&nbsp;Volver
                    </button>
                </div>
                <div class="container-fluid cz-form-box-content">
                    <div class="row paddingBotton5">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <!--CONSULTOR-->
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3" id="labelconsultor" runat="server">
                                <span class="font14-bold">Consultor</span>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9">
                                <input type="text" runat="server" class="form-control" value="" id="txtConsultor" disabled>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <!--ZONA-->
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3" id="labelzona" runat="server">

                                <span class="font14-bold">Zona</span>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9">

                                <input type="text" runat="server" class="form-control" value="" id="txtZona" disabled>
                            </div>



                        </div>
                    </div>
                    <div class="row paddingBotton5">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <!--CLIENTE-->
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                                <span class="font14-bold">Cliente</span>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9">
                                <input type="text" runat="server" class="form-control" value="" id="tCliente" disabled="disabled">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <!--MODULO-->
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                                <span class="font14-bold">M&oacute;dulo</span>

                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9">
                                <input type="text" runat="server" class="form-control" value="" id="tModulo" disabled="disabled">
                            </div>
                        </div>
                    </div>
                    <div class="row paddingBotton5">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="row paddingBotton5">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <!--VERSION SUITE-->
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">

                                        <span class="font14-bold">Suite</span>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9">

                                        <input type="text" runat="server" class="form-control" value="" id="tVersionSuiteX" placeholder="Ej: 1.0.0" />
                                    </div>
                                </div>
                            </div>
                            <div class="row paddingBotton5">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <!--VERSION WEB-->
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">

                                        <span class="font14-bold">Versi&oacute;n Web</span>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9">


                                        <input type="text" runat="server" class="form-control" id="tVersionesWeb" placeholder="Ej: 1.0.0" />
                                    </div>
                                </div>
                            </div>

                            <div class="row paddingBotton5">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <!--VERSION MOVIL-->
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">

                                        <span class="font14-bold">Versi&oacute;n S.Mobile</span>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9">
                                        <input type="text" runat="server" class="form-control" value="" id="txtVersionSerMovil" placeholder="Ej: 1.0.0" />
                                    </div>

                                </div>
                            </div>
                            <div class="row paddingBotton5">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <!--VERSION APK-->
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">

                                        <span class="font14-bold">Versi&oacute;n APK</span>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9">
                                        <input type="text" runat="server" class="form-control" value="" id="txtVersionMovil" placeholder="Ej: 1.0.0">
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <!--FECHA MODIFICACION-->
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3" id="fecha" runat="server">
                                <span class="font14-bold">Fecha Modificaci&oacute;n</span>

                                <!-- <a class='verDetalleURL' style='cursor: pointer;' urltitulo="Url Descarga APK" detalle="<%# Eval("test")%>">
                                Ver Historial de Cambios
                                </a>-->
                                <a class="editItemRegicono2" style="cursor: pointer;" data-toggle="modal">&nbsp;&nbsp; Ver Historial de Cambios 
                                </a>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9" id="texto" runat="server">
                                <input type="text" runat="server" class="form-control" value="" id="tFecMod" disabled>
                            </div>

                        </div>
                    </div>

                    <div class="row paddingBotton5">
                        <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                            <!--URL WEB-->
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2" style="margin-right: -20px;">
                                <span class="font14-bold">URL WEB</span>

                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10">
                                <input type="text" runat="server" class="form-control urlColor" value="" id="txtUrlWeb" placeholder="http://" />
                            </div>
                        </div>
                    </div>
                    <div class="row paddingBotton5">
                        <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                            <!--URL MOVIL-->
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2" style="margin-right: -20px;">
                                <span class="font14-bold">URL S.MOBILE</span>

                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10">
                                <input type="text" runat="server" class="form-control urlColor" value="" id="txtUrlMobile" placeholder="http://" />
                            </div>
                        </div>
                    </div>
                    <div class="row paddingBotton5">
                        <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                            <!--URL APK-->
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2" style="margin-right: -20px;">
                                <span class="font14-bold">URL APK</span>

                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10">
                                <input type="text" runat="server" class="form-control urlColor" value="" id="txtUrlApk" placeholder="http://" />
                            </div>
                        </div>
                    </div>
                    <div class="row paddingBotton5" style="margin-top: 5px;">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <!--HOSTING-->
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3" id="Div1" runat="server">
                                <span class="font14-bold">Hosting</span>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9">
                                <asp:Literal ID="divHosting" runat="server"></asp:Literal>
                                <%--<input type="text" runat="server" class="form-control" value="" id="Text3" />--%>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <!--NOMBRE BD-->
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3" id="Div2" runat="server">
                                <span class="font14-bold">Nombre BD</span>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9">
                                <input type="text" runat="server" class="form-control" value="" id="txtNombreBD" />
                            </div>
                        </div>
                    </div>
                    <div class="row paddingBotton5">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <!--ESTADO-->
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3" id="Div3" runat="server">
                                <span class="font14-bold">Estado Aplicación</span>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9">
                                <asp:Literal ID="divEstado" runat="server"></asp:Literal>
                                <%--<input type="text" runat="server" class="form-control" value="" id="tEstado"/>--%>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <!--SERVIDOR BD-->
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3" id="labelEstado" runat="server">
                                <span class="font14-bold">Servidor BD</span>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9">
                                <input id="txtServerBD" type="text" runat="server" class="form-control" value="" disabled="disabled" />
                            </div>
                        </div>
                    </div>
                    <div class="row paddingBotton5">
                        <div class="col-xs-12" style="word-wrap: break-word; margin-top: 20px; margin-bottom: 20px;">
                            <!--PLATAFORMA-->
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2" id="Plataforma" runat="server" style="margin-right: -52px;">
                                <span class="font14-bold">Plataforma</span>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10" id="checkBox" runat="server">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10" id="Div4" runat="server">
                                    djoapsd opasdpoasodp
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10" id="Div5" runat="server">
                                    djoapsd opasdpoasodp
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row paddingBotton5">
                        <div class="col-xs-12" style="word-wrap: break-word; margin-top: 20px; margin-bottom: 20px;">
                            <!--HOSTING-->
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2" id="hosting" runat="server" style="margin-right: -52px;">
                                <span class="font14-bold">Hosting</span>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10" id="dllHostingx" runat="server">
                                <asp:DropDownList ID="dllHosting" runat="server" class="form-control" Style="width: 400px;"></asp:DropDownList>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2" id="div" runat="server" style="margin-right: -52px;">
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10" id="txtaDatosx" runat="server">
                                <p style="color: red; font-size: small">Ingresar: Número de Contacto, IP Pública, Acceso de Teamviewer</p>
                                <textarea class="form-control" runat="server" id="txtaDatos" rows="4" style="height: 100px; width: 400px;"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row paddingBotton5">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <!--ADJUNTO 01-->
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3" id="adjunto01" runat="server">
                                <span class="font14-bold">Adjunto 01</span>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9">
                                <input type="text" runat="server" class="form-control" value="" id="tadjunto01" />
                                <a href="#">Ver Adjunto</a>
                            </div>
                        </div>
                    </div>
                    <div class="row paddingBotton5">
                        <div class="col-xs-12" style="word-wrap: break-word; margin-top: 20px; margin-bottom: 20px;">
                            <!--ALCANCE-->
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2" id="alcance" runat="server" style="margin-right: -52px;">
                                <span class="font14-bold">Alcances</span>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10" id="alcancex" runat="server">
                                <textarea class="form-control" runat="server" id="talcance" rows="4" style="height: 100px;"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row paddingBotton5">
                        <div class="col-xs-12" style="word-wrap: break-word; margin-top: 20px; margin-bottom: 20px;">
                            <!--CAMBIOS-->
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2" id="cambios" runat="server" style="margin-right: -52px;">
                                <span class="font14-bold">Cambios</span>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10" id="cambiosx" runat="server">
                                <textarea class="form-control" runat="server" id="tcambios" rows="4" style="height: 100px;"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row paddingBotton5">
                        <div class="col-xs-12" style="word-wrap: break-word; margin-top: 20px; margin-bottom: 20px;">
                            <!--RECOMENDACIONES-->
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2" id="recomendaciones" runat="server" style="margin-right: -52px;">
                                <span class="font14-bold">Recomendaciones</span>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10" id="recomendacionesx" runat="server">
                                <textarea class="form-control" runat="server" id="trecomendaciones" rows="4" style="height: 100px;"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row paddingBotton5">
                        <div class="col-xs-12" style="word-wrap: break-word; margin-top: 20px; margin-bottom: 20px;">
                            <!--COMENTARIOS-->
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2" id="comentarios" runat="server" style="margin-right: -52px;">
                                <span class="font14-bold">Detalle</span>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10" id="comentariosx" runat="server">
                                <textarea class="form-control" runat="server" id="tcomentarios" rows="4" style="height: 100px;"></textarea>
                            </div>
                        </div>
                    </div>
                    <%--<div class="row paddingBotton5">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <!--OPERADOR-->
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3" id="Div4" runat="server">
                                <span class="font14-bold">Operador Update</span>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9">
                                <input type="text" runat="server" class="form-control" value="" id="txtOperador" disabled="disabled" />
                            </div>
                        </div>
                    </div>--%>
                </div>
            </div>
            <asp:HiddenField ID="hdCodPadre" runat="server" Value="-1" />
            <asp:HiddenField ID="hdIdApp" runat="server" Value="-1" />
            <asp:HiddenField ID="hdIdTecnologia" runat="server" Value="-1" />
            <asp:HiddenField ID="hdUrlMovil" runat="server" Value="-1" />
            <input type="hidden" id="hdnIdOperador" runat="server" />
            <input type="hidden" id="hdnIdCliente" runat="server" />
            <input type="hidden" id="hdnIdPlataforma" runat="server" />
            <input type="hidden" id="hdnIdModulo" runat="server" />
            <input type="hidden" id="hidapp" runat="server" />
            <input type="hidden" id="hdnDescApp" runat="server" />
            <input type="hidden" id="hdnLogin" runat="server" />
            <div id="myModal1" class="modal-opcion hide fade" style="overflow: scroll; height: 400px; width: 560px;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                aria-hidden="true">
            </div>

            <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content" style="border-color: #000;">
                        <div class="modal-header" style="background-color: #0154a0;">
                            <button type="button" class="close" data-dismiss="modal" style="color: #fff">&times;</button>
                            <h4 class="modal-title" style="color: #fff">Limpiar Data</h4>
                        </div>
                        <div class="modal-body">
                            <p>¿Esta seguro de limpiar los campos editables?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                            <button type="button" class="btn btn-default limpiar">Si</button>
                        </div>
                    </div>

                </div>
            </div>
    </form>


</body>

</html>
<script>
    if ($('#back-to-top').length) {
        var scrollTrigger = 100, // px
            backToTop = function () {
                var scrollTop = $(window).scrollTop();
                if (scrollTop > scrollTrigger) {
                    $('#back-to-top').addClass('show');
                } else {
                    $('#back-to-top').removeClass('show');
                }
            };
        backToTop();
        $(window).on('scroll', function () {
            backToTop();
        });
        $('#back-to-top').on('click', function (e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 700);
        });
    }
</script>
