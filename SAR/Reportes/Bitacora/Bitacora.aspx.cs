﻿using Controlador;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilidad;

namespace SAR.Reportes.Bitacora
{
    public partial class Bitacora : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    //if (!String.IsNullOrEmpty(Session["lgn_id"].ToString()))
                    //{
                        //if (Session["id_perfil"].ToString() == "19")
                        //{
                        //    txtRuc.Value = Session["lgn_login"].ToString();
                        //    txtRazonSocial.Value = Session["lgn_nombre"].ToString();
                        //    txtRuc.Disabled = true;
                        //    txtRazonSocial.Disabled = true;

                        //}
                        //else
                        //{
                        //    txtRuc.Value = "";
                        //    txtRazonSocial.Value = "";
                        //}

                        //Plataforma
                        dllZona.DataSource = ControladorZona.ObtenerComboZona();
                        dllZona.DataValueField = "ID";
                        dllZona.DataTextField = "DESCRIPCION";
                        dllZona.DataBind();
                        dllZona.Items.Insert(0, ".:SELECCIONE:.");
                        dllZona.Items[0].Value = "-1";
                        //dllModulo.SelectedIndex = 3;
                        //Clientes
                        dllTipoAtencion.DataSource = ControladorTipoAtencion.ObtenerComboTipoAtencion();
                        dllTipoAtencion.DataValueField = "ID";
                        dllTipoAtencion.DataTextField = "DESCRIPCION";
                        dllTipoAtencion.DataBind();
                        dllTipoAtencion.Items.Insert(0, ".:SELECCIONE:.");
                        dllTipoAtencion.Items[0].Value = "-1";
                        //Consultores
                        //StringBuilder sb = new StringBuilder();
                        //if (Session["id_perfil"].ToString() == BeanConstantes.strPerfilGerente || Session["id_perfil"].ToString() == BeanConstantes.strPerfilOperador || Session["id_perfil"].ToString() == BeanConstantes.strPerfilPT || Session["id_perfil"].ToString() == "3" || Session["id_perfil"].ToString() == BeanConstantes.strPerfilCoordinador)
                        //{
                        //    DataTable dt = ControladorUsuario.fnSelConsultores(Convert.ToInt32(Session["lgn_id"]), Session["id_perfil"].ToString(), Convert.ToInt32(Session["lgn_idZona"]));
                        //    sb.Append("<div class='cz-form-content'>");
                        //    sb.Append("<p>Consultor</p>");
                        //    sb.Append("<select name='dllConsultor' id='dllConsultor' class='form-control'>");
                        //    sb.Append("<option value='-1'> TODOS </option>");
                        //    foreach (DataRow drItem in dt.Rows)
                        //    {
                        //        sb.Append("<option value='" + drItem["IdConsultor"] + "'>" + drItem["DESCRIPCION"] + "</option>");
                        //    }
                        //    sb.Append("</select>");
                        //    sb.Append("</div>");
                        //}
                        //else
                        //{
                        //    sb.Append("<input type='hidden' name='dllConsultor' id='dllConsultor' value='" + Session["id_perfil"].ToString() + "'>");
                        //}
                        //litDllConsultor.Text = sb.ToString();
                    //}
                    //else
                    //{
                    //    string myScript = "parent.document.location.href = '../../default.aspx?acc=EXT';";
                    //    Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", myScript, true);
                    //}
                }
            }
            catch (Exception ex)
            {
                Utility.registrarLog("Error - Ventana Aplicaciones.aspx.cs: " + ex.Message);
                string myScript = "parent.document.location.href = '../../default.aspx?acc=ERR';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", myScript, true);
            }
        }
    }
}