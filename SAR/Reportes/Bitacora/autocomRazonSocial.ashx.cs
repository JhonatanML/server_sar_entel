﻿using Modelo.BEANS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace SAR.Reportes.Bitacora
{
    /// <summary>
    /// Summary description for autocomRazonSocial
    /// </summary>
    public class autocomRazonSocial : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string match = context.Request["q"];
            DataTable ds = Controlador.ControladorRazonSocial.ObtenerRazonSocialBitacora(match);

            List<BeanAutoComp> instituciones = new List<BeanAutoComp>();

            if (ds.Rows.Count > 0)
            {
                BeanAutoComp institucion;

                foreach (DataRow dr in ds.Rows)
                {
                    institucion = new BeanAutoComp();
                    institucion.vNombre = dr["RS"].ToString();
                    instituciones.Add(institucion);
                }
            }
            string output = Newtonsoft.Json.JsonConvert.SerializeObject(instituciones, Newtonsoft.Json.Formatting.Indented);
            context.Response.Write(output);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}