﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Bitacora.aspx.cs" Inherits="SAR.Reportes.Bitacora.Bitacora" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--CSS-->
    <script src="../../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../js/jsmodule.js" type="text/javascript"></script>
    <script src="../../js/Validacion.js" type="text/javascript"></script>
    <link href="../../css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="../../js/cz_main.js" type="text/javascript"></script>
    <link href="../../css/styleBootStrap.css" rel="stylesheet" />
    <script src="../../js/jquery.autocomplete.js"></script>
    <link href="../../css/jquery.autocomplete.css" rel="stylesheet" />

    <link href="../../css/datepicker.css" rel="stylesheet" />
    <link href="../../css/bootstrap-date.css" rel="stylesheet" />
    <script>
        var urlbus = 'Grid.aspx';
        $(document).ready(function () {
            buscarApp();
            $('#buscar').trigger("click");
            $('.solo-numero').keyup(function () {
                this.value = (this.value + '').replace(/[^0-9]/g, '');
            });

            if ($("#dllConsultor").val() == '19') {
                $("#padre").hide();
                $("#modulo").hide();
                $("#web").hide();
                $("#descarga").hide();
                $("#tecnologia").hide();
                $("#estado").hide();
                $("#boton").hide();
            }
            else {
                $("#padre").show();
                $("#modulo").show();
                $("#web").show();
                $("#descarga").show();
                $("#tecnologia").show();
                $("#estado").show();
                $("#boton").show();
            }


        });

        function getParametros() {
            var strData = new Object();
            strData.ruc = $('#txtRuc').val();
            strData.razonSocial = $('#txtRazonSocial').val();
            if ($('#dllConsultor').val() == '19') {

                strData.tecnologia = '3';
                strData.codPadre = "";
                strData.modulo = "";
                strData.urlSite = "";
                strData.urlApk = "";
                strData.idConsultor = $('#dllConsultor').val();
                strData.estado = '2';
                strData.pagina = $('#hdnActualPage').val();
                strData.filas = $('#hdnShowRows').val();
                strData.where = 'APP';
            }
            else {
                strData.tecnologia = $('#dllTecnologia').val();
                strData.codPadre = $('#txtCodPadre').val();
                strData.modulo = $('#txtModulo').val();
                strData.urlSite = $('#txtUrlSite').val();
                strData.urlApk = $('#txtUrlApk').val();
                strData.idConsultor = '-1'; //$('#dllConsultor').val();
                strData.estado = $('#dllEstado').val();
                strData.pagina = $('#hdnActualPage').val();
                strData.filas = $('#hdnShowRows').val();
                strData.where = 'APP';
            }

            return strData;
        }
    </script>
    <style>
        .input-Text{
            height: 20px;
            width: 175px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="cz-submain cz-submain-form-background">
            <div id="cz-form-box">
                <div class="cz-form-box-content">
                    <div id="cz-form-box-content-title" style="margin-top: -12px;">
                        <div id="cz-form-box-content-title-text">
                            <span class="glyphicon glyphicon-export btn-lg cz-form-box-content-title-icon" style="color: #666;" aria-hidden="true"></span>
                            <span class="font-size-20">Busqueda de Aplicaci&oacute;n</span>
                        </div>
                    </div>
                </div>
                <div class="cz-form-box-content">
                    <div class="cz-form-content" style="width:385px">
                        <p>Raz&oacute;n Social</p>
                        <input type="text" class="requerid form-control input-Text" runat="server" id="txtRazonSocial" style="width:330px">
                    </div>
                    <div class="cz-form-content">
                        <p>Zona</p>
                        <asp:DropDownList ID="dllZona" runat="server" class="form-control"></asp:DropDownList>
                    </div>
                    <div class="cz-form-content">
                        <p>Fecha Inicio Atención</p>
                        <asp:TextBox ID="txtFechaCreacion1" runat="server" class="cz-form-content-input-text" ReadOnly="true"></asp:TextBox>
                        <div class="cz-form-content-input-text-visible"></div>
                        <%--<input type="hidden" id="txtFechaEnviar1"/>--%>
                    </div>
                    <div class="cz-form-content">
                        <p>Fecha Fin Atención</p>
                        <asp:TextBox ID="txtFechaCreacion2" runat="server" class="cz-form-content-input-text" ReadOnly="true"></asp:TextBox>
                        <div class="cz-form-content-input-text-visible"></div>
                        <%--<input type="hidden" id="txtFechaEnviar2"/>--%>
                    </div>
                    <div class="cz-form-content">
                        <p>Tipo Atención</p>
                        <asp:DropDownList ID="dllTipoAtencion" runat="server" class="form-control"></asp:DropDownList>
                    </div>
                    <%--<div class="cz-form-content">
                        <p>RUC</p>
                        <input type="text" class="requerid form-control input-Text solo-numero" runat="server" id="txtRuc" maxlength="11">
                    </div>
                    <div class="cz-form-content" id="padre" runat="server" visible="false">
                        <p>C&oacute;digo Padre</p>
                        <input type="text" class="requerid form-control input-Text"  id="txtCodPadre">
                    </div>
                    <div class="cz-form-content" id="modulo" style="width:370px" >
                        <p>M&oacute;dulo</p>
                        <input type="text" class="requerid form-control input-Text"  id="txtModulo" style="width:332px">
                    </div>
                     <div class="cz-form-content" id="web" style="width:600px" >
                        <p>Url Web</p>
                        <input type="text" class="requerid form-control input-Text"  id="txtUrlSite" style="width:550px">
                    </div>
                     <div class="cz-form-content" id="descarga" style="width:600px" >
                        <p>Url Descarga APK</p>
                        <input type="text" class="requerid form-control input-Text" runat="server" id="txtUrlApk" style="width:550px">
                    </div>
                    <div class="cz-form-content" id="tecnologia" >
                        <p>Tecnolog&iacute;a</p>
                        <asp:DropDownList ID="dllTecnologia" runat="server" class="form-control"></asp:DropDownList>
                    </div>
                    
                    <%--<asp:Literal ID="litDllConsultor" runat="server"></asp:Literal>--%>
                    <%--<div class="cz-form-content" id="estado" runat="server">
                        <p>Estado</p>
                        <asp:DropDownList ID="dllEstado" runat="server" class="form-control"></asp:DropDownList>
                    </div>--%>
                    <div class="cz-form-content cz-util-right" id="boton" runat="server">
                        <button id="buscar" type="button" class="cz-form-content-input-button btn-lg cz-util-right" style="margin-top: 10px; width: 100%;">
                            Buscar
                        </button>
                    </div>
                </div>
                <div class="cz-form-box-content">
                    <div class="form-grid-box">
                        <div class="form-grid-table-outer">
                            <div class="form-grid-table-inner">
                                <div class="form-gridview-data" id="divGridViewData" runat="server"></div>
                                <div class="form-gridview-error" id="divGridViewError" runat="server"></div>
                                <div class="form-gridview-search" id="divGridViewSearch" runat="server">
                                    <img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />
                                    <p>Buscando Resultados</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="paginator-hidden-fields">
                    <asp:HiddenField ID="hdnActualPage" Value="1" runat="server" />
                    <asp:HiddenField ID="hdnShowRows" Value="10" runat="server" />
                    <asp:HiddenField ID="hdRazonSocial" Value="" runat="server" />
                    <asp:HiddenField ID="hdperfil" Value="" runat="server" />

                </div>
                <%--<div id="myModalCargando" class="modal hide fade in" style="display: block; background: transparent; border: none; -webkit-box-shadow: none;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
                </div>
                <div id="myModal" class="modal hide fade" style="display: block;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                </div>--%>
            </div>
        </div>
    </form>
         <script>

             $(document).ready(function () {

                 $('#txtRazonSocial').autocomplete('autocomRazonSocial.ashx',
                         {
                             multiple: false,
                             minChars: 3,
                             max: 12,
                             autoFill: false,
                             cacheLength: 0,
                             mustMatch: true,
                             matchContains: false,
                             //selectFirst: false,
                             dataType: 'json',
                             parse: function (data) {
                                 return $.map(data, function (row) {
                                     return {
                                         data: row, value: row.vNombre, result: row.vNombre
                                     }
                                 });
                             },
                             formatItem: function (item) {
                                 return item.vNombre;
                             }

                         });

                         //$('#txtModulo').autocomplete('autocomModulo.ashx',
                         //{
                         //    multiple: false,
                         //    minChars: 3,
                         //    max: 12,
                         //    autoFill: false,
                         //    cacheLength: 0,
                         //    mustMatch: true,
                         //    matchContains: false,
                         //    //selectFirst: false,
                         //    dataType: 'json',
                         //    parse: function (data) {
                         //        return $.map(data, function (row) {
                         //            return {
                         //                data: row, value: row.vNombre, result: row.vNombre
                         //            }
                         //        });
                         //    },
                         //    formatItem: function (item) {
                         //        return item.vNombre;
                         //    }

                         //});

                         //$('#txtUrlSite').autocomplete('autocomUrlSite.ashx',
                         //{
                         //    multiple: false,
                         //    minChars: 3,
                         //    max: 12,
                         //    autoFill: false,
                         //    cacheLength: 0,
                         //    mustMatch: true,
                         //    matchContains: false,
                         //    //selectFirst: false,
                         //    dataType: 'json',
                         //    parse: function (data) {
                         //        return $.map(data, function (row) {
                         //            return {
                         //                data: row, value: row.vNombre, result: row.vNombre
                         //            }
                         //        });
                         //    },
                         //    formatItem: function (item) {
                         //        return item.vNombre;
                         //    }

                         //});

                         //$('#txtUrlApk').autocomplete('autocomUrlApk.ashx',
                         //{
                         //    multiple: false,
                         //    minChars: 3,
                         //    max: 12,
                         //    autoFill: false,
                         //    cacheLength: 0,
                         //    mustMatch: true,
                         //    matchContains: false,
                         //    //selectFirst: false,
                         //    dataType: 'json',
                         //    parse: function (data) {
                         //        return $.map(data, function (row) {
                         //            return {
                         //                data: row, value: row.vNombre, result: row.vNombre
                         //            }
                         //        });
                         //    },
                         //    formatItem: function (item) {
                         //        return item.vNombre;
                         //    }

                         //});
             });

    </script>
    <script src="../../js/bootstrap-datepicker.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#txtFechaCreacion1').datepicker({
                format: "dd/mm/yyyy",
                autoclose: true,
                clearBtn: true
            });
            $('#txtFechaCreacion2').datepicker({
                format: "dd/mm/yyyy",
                autoclose: true,
                clearBtn: true
            });

            $('#txtFechaCreacion1').change(function () {
                var fechaBefore = $('#txtFechaCreacion1').val().split('/');
                fechaBefore.reverse();
                var fechaAfter = fechaBefore.join('-');
                $('#txtFechaEnviar1').val(fechaAfter);
            });
            $('#txtFechaCreacion2').change(function () {
                var fechaBefore = $('#txtFechaCreacion2').val().split('/');
                fechaBefore.reverse();
                var fechaAfter = fechaBefore.join('-');
                $('#txtFechaEnviar2').val(fechaAfter);
            });
        });
    </script>
</body>
</html>