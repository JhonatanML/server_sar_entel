﻿using Controlador;
using Modelo.BEANS;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace SAR.Reportes.Requerimientos
{
    /// <summary>
    /// Descripción breve de autocomRazonSocial
    /// </summary>
    public class autocomRazonSocial : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context/*, HttpContext context2/*R*/)
        {

            string match = context.Request["q"];
            String idConsultor = context.Request.QueryString["c"];
            //string json = new System.IO.StreamReader(context.Request.InputStream).ReadToEnd();
            //Dictionary<string, string> JSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            //String match = Convert.ToString(JSON["tsEmpNum"]);
            //String idConsultor = Convert.ToString(JSON["term"]);
            DataTable ds = ControladorCliente.fnSelRazonSocial(match, idConsultor);

            List<BeanAutoComp> instituciones = new List<BeanAutoComp>();

            if (ds.Rows.Count > 0)
            {
                BeanAutoComp institucion;

                foreach (DataRow dr in ds.Rows)
                {
                    institucion = new BeanAutoComp();
                    institucion.vNombre = dr["RS"].ToString();
                    instituciones.Add(institucion);
                }
            }
            string output = Newtonsoft.Json.JsonConvert.SerializeObject(instituciones, Newtonsoft.Json.Formatting.Indented);
            context.Response.Write(output);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}