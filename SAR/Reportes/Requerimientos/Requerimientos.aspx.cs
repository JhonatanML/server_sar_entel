﻿using Controlador;
using Modelo.BEANS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilidad;

namespace SAR.Reportes.Requerimientos
{
    public partial class Requerimientos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    BeanUsuario beanSesion = (BeanUsuario)Session[Configuracion.SESSION_OBJ_USUARIO];
                    if (!String.IsNullOrEmpty(beanSesion.login))
                    {
                        CargarCombos(beanSesion);
                        hdnIdUsuario.Value = beanSesion.id;
                        hdnIdPerfil.Value = beanSesion.id_perfil;
                        //Plataforma
                        //dllTecnologia.DataSource = ControladorTecnologia.Tecnologia_dt_Combo();
                        //dllTecnologia.DataValueField = "IDE";
                        //dllTecnologia.DataTextField = "DESCRIPCION";
                        //dllTecnologia.DataBind();
                        //dllTecnologia.Items.Insert(0, " TODOS ");
                        //dllTecnologia.Items[0].Value = "-1";
                        //dllTecnologia.SelectedIndex = 3;
                        //Clientes
                        //dllEstado.DataSource = ControladorEstado.Estado_dt_Combo();
                        //dllEstado.DataValueField = "IDE";
                        //dllEstado.DataTextField = "EM";
                        //dllEstado.DataBind();
                        //dllEstado.Items.Insert(0, " TODOS ");
                        //dllEstado.Items[0].Value = "-1";
                        //Consultores
                        //StringBuilder sb = new StringBuilder();
                        //if (Session["id_perfil"].ToString() == BeanConstantes.strPerfilGerente || Session["id_perfil"].ToString() == BeanConstantes.strPerfilOperador || Session["id_perfil"].ToString() == BeanConstantes.strPerfilPT || Session["id_perfil"].ToString() == "3" || Session["id_perfil"].ToString() == BeanConstantes.strPerfilCoordinador)
                        //{
                        //    DataTable dt = ControladorUsuario.fnSelConsultores(Convert.ToInt32(Session["lgn_id"]), Session["id_perfil"].ToString(), Convert.ToInt32(Session["lgn_idZona"]));
                        //    sb.Append("<div class='cz-form-content'>");
                        //    sb.Append("<p>Consultor</p>");
                        //    sb.Append("<select name='dllConsultor' id='dllConsultor' class='form-control'>");
                        //    sb.Append("<option value='-1'> TODOS </option>");
                        //    foreach (DataRow drItem in dt.Rows)
                        //    {
                        //        sb.Append("<option value='" + drItem["IdConsultor"] + "'>" + drItem["DESCRIPCION"] + "</option>");
                        //    }
                        //    sb.Append("</select>");
                        //    sb.Append("</div>");
                        //}
                        //else
                        //{
                        //    sb.Append("<input type='hidden' name='dllConsultor' id='dllConsultor' value='" + Session["id_perfil"].ToString() + "'>");
                        //}
                        //litDllConsultor.Text = sb.ToString();
                    }
                    else
                    {
                        string myScript = "parent.document.location.href = '../../default.aspx?acc=EXT';";
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", myScript, true);
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.registrarLog("Error - Ventana Aplicaciones.aspx.cs: " + ex.Message);
                string myScript = "parent.document.location.href = '../../default.aspx?acc=ERR';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", myScript, true);
            }
        }

        private void CargarCombos(BeanUsuario beanSesion)
        {
            //Zona
            ddlZona.DataSource = ControladorZona.ObtenerComboZona();
            ddlZona.DataValueField = "ID";
            ddlZona.DataTextField = "DESCRIPCION";
            ddlZona.DataBind();
            ddlZona.Items.Insert(0, " TODOS ");
            ddlZona.Items[0].Value = "-1";

            //Consultores
            ddlConsultores.DataSource = ControladorUsuario.ObtenerConsultores(Convert.ToInt32(beanSesion.id), beanSesion.id_perfil, "-1");
            ddlConsultores.DataValueField = "IdConsultor";
            ddlConsultores.DataTextField = "DESCRIPCION";
            ddlConsultores.DataBind();
            ddlConsultores.Items.Insert(0, " TODOS ");
            ddlConsultores.Items[0].Value = "-1";

            ////Consultores
            //StringBuilder sb = new StringBuilder();
            ////if (Session["id_perfil"].ToString() == BeanConstantes.strPerfilGerente || Session["id_perfil"].ToString() == BeanConstantes.strPerfilOperador || Session["id_perfil"].ToString() == BeanConstantes.strPerfilPT || Session["id_perfil"].ToString() == "3" || Session["id_perfil"].ToString() == BeanConstantes.strPerfilCoordinador)
            ////{
            //DataTable dt = ControladorUsuario.ObtenerConsultores(Convert.ToInt32(beanSesion.id), beanSesion.id_perfil, "-1");
            //sb.Append("<div class='cz-form-content'>");
            //    sb.Append("<p>Consultor</p>");
            //    sb.Append("<select name='dllConsultor' id='dllConsultor' class='form-control'>");
            //    sb.Append("<option value='-1'> TODOS </option>");
            //    foreach (DataRow drItem in dt.Rows)
            //    {
            //        sb.Append("<option value='" + drItem["IdConsultor"] + "'>" + drItem["DESCRIPCION"] + "</option>");
            //    }
            //    sb.Append("</select>");
            //    sb.Append("</div>");
            //}
            //else
            //{
            //    sb.Append("<input type='hidden' name='dllConsultor' id='dllConsultor' value='" + Session["id_perfil"].ToString() + "'>");
            //}
            //litDllConsultor.Text = sb.ToString();
        }

        //[WebMethod]
        //public static String ObtenerConsultoresDinamico(String idZona, String idUsuario, String idPerfil)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    //if (Session["id_perfil"].ToString() == BeanConstantes.strPerfilGerente || Session["id_perfil"].ToString() == BeanConstantes.strPerfilOperador || Session["id_perfil"].ToString() == BeanConstantes.strPerfilPT || Session["id_perfil"].ToString() == "3" || Session["id_perfil"].ToString() == BeanConstantes.strPerfilCoordinador)
        //    //{
        //    DataTable dt = ControladorUsuario.ObtenerConsultores(Convert.ToInt32(idUsuario), idPerfil, idZona);
        //    sb.Append("<div class='cz-form-content'>");
        //    sb.Append("<p>Consultor</p>");
        //    sb.Append("<select name='dllConsultor' id='dllConsultor' class='form-control'>");
        //    sb.Append("<option value='-1'> TODOS </option>");
        //    foreach (DataRow drItem in dt.Rows)
        //    {
        //        sb.Append("<option value='" + drItem["IdConsultor"] + "'>" + drItem["DESCRIPCION"] + "</option>");
        //    }
        //    sb.Append("</select>");
        //    sb.Append("</div>");

        //    return sb.ToString();

        //}

        [WebMethod]
        public static string ObtenerConsultoresDinamico(String idZona, String idUsuario, String idPerfil)
        {
            try
            {
                List<BeanUsuario> lstBean = new List<BeanUsuario>();
                lstBean = ControladorUsuario.ObtenerConsultoresDinamico(Convert.ToInt32(idUsuario), idPerfil, idZona);

                return Newtonsoft.Json.JsonConvert.SerializeObject(lstBean);
            }
            catch (Exception ex)
            {
                Utility.registrarLog("Error CargarListaServicios(): " + ex.Message);
                Utility.registrarLog("Error CargarListaServicios(): " + ex.StackTrace);
                throw new Exception("ERROR: " + ex.Message);
            }
        }
    }
}