﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Requerimientos.aspx.cs" Inherits="SAR.Reportes.Requerimientos.Requerimientos" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--CSS-->
    <script src="../../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../js/jsmodule.js" type="text/javascript"></script>
    <script src="../../js/Validacion.js" type="text/javascript"></script>
    <link href="../../css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="../../js/cz_main.js" type="text/javascript"></script>
    <link href="../../css/styleBootStrap.css" rel="stylesheet" />
    <script src="../../js/jquery.autocomplete.js"></script>
    <link href="../../css/jquery.autocomplete.css" rel="stylesheet" />
    <script>
        var urlbus = 'Grid.aspx';
        var urlGetConsultores = "Requerimientos.aspx/ObtenerConsultoresDinamico";
        $(document).ready(function () {
            //buscarApp();
            ObtenerConsultoresRequerimientos();
            $('#buscar').trigger("click");
            $('.solo-numero').keyup(function () {
                this.value = (this.value + '').replace(/[^0-9]/g, '');
            });




        });

        function getParametros() {


            return strData;
        }
    </script>
    <style>
        .input-Text {
            height: 20px;
            width: 175px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <input type="hidden" id="hdnIdUsuario" runat="server" />
        <input type="hidden" id="hdnIdPerfil" runat="server" />
        <input type="hidden" id="hdnIdConsultor" runat="server"/>
        <div class="cz-submain cz-submain-form-background">
            <div id="cz-form-box">
                <div class="cz-form-box-content">
                    <div id="cz-form-box-content-title" style="margin-top: -12px;">
                        <div id="cz-form-box-content-title-text">
                            <span class="glyphicon glyphicon-list-alt btn-lg cz-form-box-content-title-icon" style="color: #666;" aria-hidden="true"></span>
                            <span class="font-size-20">Reporte de Requerimientos</span>
                        </div>
                    </div>
                </div>
                <div class="cz-form-box-content">
                    <div class="cz-form-content">
                        <p>Zona</p>
                        <asp:DropDownList ID="ddlZona" runat="server" class="form-control"></asp:DropDownList>
                        <%--<input type="text" class="requerid form-control input-Text solo-numero" runat="server" id="txtRuc" maxlength="11">--%>
                    </div>

                    <div class="cz-form-content">
                        <p>Consultor:</p>
                        <asp:DropDownList ID="ddlConsultores" runat="server" class="form-control"></asp:DropDownList>
                    </div>

                    <div class="cz-form-content" style="width: 385px">
                        <p>Raz&oacute;n Social</p>
                        <input type="text" class="requerid form-control input-Text" runat="server" id="txtRazonSocial" style="width: 330px">
                    </div>
                    <div class="cz-form-content" id="padre" runat="server" visible="false">
                        <p>C&oacute;digo Padre</p>
                        <input type="text" class="requerid form-control input-Text" id="txtCodPadre">
                    </div>
                    <div class="cz-form-content" id="modulo" style="width: 370px">
                        <p>M&oacute;dulo</p>
                        <input type="text" class="requerid form-control input-Text" id="txtModulo" style="width: 332px">
                    </div>
                    <div class="cz-form-content" id="web" style="width: 600px">
                        <p>Url Web</p>
                        <input type="text" class="requerid form-control input-Text" id="txtUrlSite" style="width: 550px">
                    </div>
                    <div class="cz-form-content" id="descarga" style="width: 600px">
                        <p>Url Descarga APK</p>
                        <input type="text" class="requerid form-control input-Text" runat="server" id="txtUrlApk" style="width: 550px">
                    </div>
                    <div class="cz-form-content" id="tecnologia">
                        <p>Tecnolog&iacute;a</p>
                        <asp:DropDownList ID="dllTecnologia" runat="server" class="form-control"></asp:DropDownList>
                    </div>

                    <%--<div class="cz-form-content">
                        <p>Consultor:</p>
                        <asp:DropDownList ID="ddlConsultores" runat="server" class="form-control"></asp:DropDownList>
                    </div>--%>

                    <%--<asp:Literal ID="litDllConsultor" runat="server"></asp:Literal>--%>
                    <div class="cz-form-content" id="estado" runat="server">
                        <p>Estado</p>
                        <asp:DropDownList ID="dllEstado" runat="server" class="form-control"></asp:DropDownList>
                    </div>
                    <div class="cz-form-content cz-util-right" id="boton" runat="server">
                        <button id="buscar" type="button" class="cz-form-content-input-button btn-lg cz-util-right" style="margin-top: 10px; width: 100%;">
                            Buscar
                        </button>
                    </div>
                </div>
                <div class="cz-form-box-content">
                    <div class="form-grid-box">
                        <div class="form-grid-table-outer">
                            <div class="form-grid-table-inner">
                                <div class="form-gridview-data" id="divGridViewData" runat="server"></div>
                                <div class="form-gridview-error" id="divGridViewError" runat="server"></div>
                                <div class="form-gridview-search" id="divGridViewSearch" runat="server">
                                    <img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />
                                    <p>Buscando Resultados</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="paginator-hidden-fields">
                    <asp:HiddenField ID="hdnActualPage" Value="1" runat="server" />
                    <asp:HiddenField ID="hdnShowRows" Value="10" runat="server" />
                    <asp:HiddenField ID="hdRazonSocial" Value="" runat="server" />
                    <asp:HiddenField ID="hdperfil" Value="" runat="server" />

                </div>
                <div id="myModalCargando" class="modal hide fade in" style="display: block; background: transparent; border: none; -webkit-box-shadow: none;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
                </div>
                <div id="myModal" class="modal hide fade" style="display: block;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                </div>
            </div>
        </div>
    </form>
    <script>

        $(document).ready(function () {

            $('#txtRazonSocial').autocomplete('autocomRazonSocial.ashx?c=' + $("#hdnIdConsultor").val(),
            {
                multiple: false,
                minChars: 3,
                max: 12,
                autoFill: false,
                cacheLength: 0,
                mustMatch: true,
                matchContains: false,
                //selectFirst: false,
                dataType: 'json',
                parse: function (data) {
                    return $.map(data, function (row) {
                        return {
                            data: row, value: row.vNombre, result: row.vNombre
                        }
                    });
                },
                formatItem: function (item) {
                    return item.vNombre;
                }

            });

            //$('#txtRazonSocial').autocomplete(



            //    //'autocomRazonSocial.ashx?c=-1'
            //    $.ajax({
            //        URL: 'autocomRazonSocial.ashx',
            //        dataType: 'json',
            //        type: 'POST',
            //        data: {
            //            tsEmpNum: $('#hdnIdConsultor').val(),
            //            term: request.term
            //        }
            //    })
            //        ,
            //        {
            //            multiple: false,
            //            minChars: 3,
            //            max: 12,
            //            autoFill: false,
            //            cacheLength: 0,
            //            mustMatch: true,
            //            matchContains: false,
            //            //selectFirst: false,
            //            dataType: 'json',
            //            parse: function (data) {
            //                return $.map(data, function (row) {
            //                    return {
            //                        data: row, value: row.vNombre, result: row.vNombre
            //                    }
            //                });
            //            },
            //            formatItem: function (item) {
            //                return item.vNombre;
            //            }

            //        });

            //$("#txtRazonSocial").autocomplete({
            //    source: function (request, response) {
            //        $.ajax({
            //            url: "autocomRazonSocial.ashx",
            //            dataType: "jsonp",
            //            data: {
            //                q: request.term,
            //                c: $("#hdnIdConsultor").val()
            //            },
            //            success: function (data) {
            //                response(data);
            //            }
            //        });
            //    },
            //    minLength: 3,
            //    select: function (event, ui) {
            //        log(ui.item ?
            //          "Selected: " + ui.item.label :
            //          "Nothing selected, input was " + this.value);
            //    },
            //    open: function () {
            //        $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
            //    },
            //    close: function () {
            //        $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
            //    }
            //});

            //$("#country_autocomplete").autocomplete({
            //    source: function (request, response) {
            //        var continent = $("#continent_autocomplete").val()
            //            , country = request.term;

            //        $.ajax({
            //            url: '/db/country.php?continent=' + continent + "&country=" + country,
            //            success: function (data) {
            //                response(parseLineSeperated(data));
            //            },
            //            error: function (req, str, exc) {
            //                alert(str);
            //            }
            //        });
            //    }
            //});

            $('#txtModulo').autocomplete('autocomModulo.ashx',
            {
                multiple: false,
                minChars: 3,
                max: 12,
                autoFill: false,
                cacheLength: 0,
                mustMatch: true,
                matchContains: false,
                //selectFirst: false,
                dataType: 'json',
                parse: function (data) {
                    return $.map(data, function (row) {
                        return {
                            data: row, value: row.vNombre, result: row.vNombre
                        }
                    });
                },
                formatItem: function (item) {
                    return item.vNombre;
                }

            });

            $('#txtUrlSite').autocomplete('autocomUrlSite.ashx',
            {
                multiple: false,
                minChars: 3,
                max: 12,
                autoFill: false,
                cacheLength: 0,
                mustMatch: true,
                matchContains: false,
                //selectFirst: false,
                dataType: 'json',
                parse: function (data) {
                    return $.map(data, function (row) {
                        return {
                            data: row, value: row.vNombre, result: row.vNombre
                        }
                    });
                },
                formatItem: function (item) {
                    return item.vNombre;
                }

            });

            $('#txtUrlApk').autocomplete('autocomUrlApk.ashx',
            {
                multiple: false,
                minChars: 3,
                max: 12,
                autoFill: false,
                cacheLength: 0,
                mustMatch: true,
                matchContains: false,
                //selectFirst: false,
                dataType: 'json',
                parse: function (data) {
                    return $.map(data, function (row) {
                        return {
                            data: row, value: row.vNombre, result: row.vNombre
                        }
                    });
                },
                formatItem: function (item) {
                    return item.vNombre;
                }

            });
        });

    </script>
</body>
</html>

