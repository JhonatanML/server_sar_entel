﻿using Controlador;
using Modelo.BEANS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilidad;

namespace SAR
{
    public partial class Inicio : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                StringBuilder sbmenuinicio = new StringBuilder();
                String OldcategoriaRef = "";
                BeanUsuario beanSesion = (BeanUsuario)Session[Configuracion.SESSION_OBJ_USUARIO];
                sp_NombreUsuario.InnerText = beanSesion.nombre != null ? "BIENVENIDO " + beanSesion.nombre : "";
                List<BeanPermisos> lista = ControladorUsuario.permisosUsuario(beanSesion.id_perfil != null ? beanSesion.id_perfil : "0", beanSesion.id);
                if (lista.Count > 0)
                {
                    foreach (BeanPermisos bean in lista)
                    {
                        if (OldcategoriaRef != bean.Referencia)
                        {
                            if (OldcategoriaRef != "")
                            {
                                sbmenuinicio.Append("</div></div>");
                            }

                            //Instanciando categoria de menu
                            sbmenuinicio.Append("<div class=\"cz-other-box-center-box\">");
                            sbmenuinicio.Append("<div class=\"cz-other-box-center-box-title\">" + bean.NombreCategoria + "</div>");
                            sbmenuinicio.Append("<div class=\"cz-other-box-center-box-options\">");

                            OldcategoriaRef = bean.Referencia;
                        }
                        //Instanciando opcion de menu
                        sbmenuinicio.Append("<div class=\"cz-other-box-center-box-option\"> <a href=\"" + bean.Url + "\">" + bean.NombreOpcion + "</a> </div>");

                    }

                    //Instanciando final de categoria de menu
                    sbmenuinicio.Append("</div></div>");
                }


                MenuInicio.Text = sbmenuinicio.ToString();
            }
            catch (Exception ex)
            {
                string myScript = "parent.document.location.href = 'Default.aspx?acc=EXT';";
                Utility.registrarLog("< ERROR [MENU.ASPX.CS]:> " + ex.Message);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", myScript, true);
            }
        }
    }
}