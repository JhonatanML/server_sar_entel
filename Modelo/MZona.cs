﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Utilidad;

namespace Modelo
{
    public class MZona
    {
        public static DataTable ObtenerComboZona()
        {
            ArrayList alParameters = new ArrayList();
            return SqlConnector.getDataTable("SAR_SPS_OBTENER_ZONA", alParameters);
        }
    }
}
