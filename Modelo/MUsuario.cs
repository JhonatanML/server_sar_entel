﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Utilidad;

namespace Modelo
{
    public class MUsuario
    {
        public static DataTable validarUsuario(String lgn, String pwd)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@cod_usuario", SqlDbType.VarChar, 50);
            parameter.Value = lgn;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@pass_usuario", SqlDbType.VarChar, 50);
            parameter.Value = pwd;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("SAR_SPS_VALIDARUSUARIO", alParameters);

        }

        public static DataTable permisosUsuario(String idPerfil, String idUSuario)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@ID_PERFIL", SqlDbType.Int);
            parameter.Value = idPerfil;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@ID_USUARIO", SqlDbType.Int);
            parameter.Value = idUSuario;
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("SAR_SPS_WEB_PERMISOS_MENU", alParameters);
        }

        public static DataTable ObtenerConsultores(Int32 piIdUsuario, String psCodigoPerfil, String piIdZona)
        {
            ArrayList loArrParametros = new ArrayList();
            SqlParameter loParametro;

            loParametro = new SqlParameter("@IdUsuario", SqlDbType.Int);
            loParametro.Value = piIdUsuario;
            loArrParametros.Add(loParametro);

            loParametro = new SqlParameter("@CodigoPerfil", SqlDbType.VarChar, 10);
            loParametro.Value = psCodigoPerfil;
            loArrParametros.Add(loParametro);

            loParametro = new SqlParameter("@IdZona", SqlDbType.VarChar, 10);
            loParametro.Value = piIdZona;
            loArrParametros.Add(loParametro);

            return SqlConnector.getDataTable("SAR_SPS_CONSULTORES", loArrParametros);
        }
    }
}
