﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Utilidad;

namespace Modelo
{
    public class MCliente
    {
        public static DataTable fnSelRazonSocial(String psRazonSocial, String idConsultor)
        {
            ArrayList loArrParametros = new ArrayList();
            SqlParameter loParametro;

            loParametro = new SqlParameter("@SEARCH", SqlDbType.VarChar, 100);
            loParametro.Value = psRazonSocial;
            loArrParametros.Add(loParametro);

            loParametro = new SqlParameter("@ID_CONSULTOR", SqlDbType.VarChar, 100);
            loParametro.Value = idConsultor;
            loArrParametros.Add(loParametro);


            return SqlConnector.getDataTable("SAR_SPS_SELRAZONSOCIAL", loArrParametros);
        }
    }
}
