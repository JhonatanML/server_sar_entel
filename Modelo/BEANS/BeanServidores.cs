﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.BEANS
{
    public class BeanServidores
    {
        public Int32 idServidor { get; set; }
        public String IpServidor { get; set; }
        public String nombreServidor { get; set; }
        public String fechaCreacion { get; set; }
        public String estado { get; set; }
        public String version { get; set; }
        public BeanServidores() { }
    }
}
