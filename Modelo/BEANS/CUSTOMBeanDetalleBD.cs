﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.BEANS
{
    public class CUSTOMBeanDetalleBD
    {
        public List<BeanDetalleBD> lstDetalleBD { get; set; }
        public Int32 id_Auditoria { get; set; }
        public String ip_Servidor { get; set; }
        public String nombreServidor { get; set; }
        public String nombre_DB { get; set; }
        public String size_DB { get; set; }
        public String ownerDB { get; set; }
        public String fechaCreacionDB { get; set; }
        public String fechaUltimoUso { get; set; }
        public String observacion { get; set; }
        public String Antiguedad { get; set; }
        public String TiempoDesuso { get; set; }
        public String estado { get; set; }
        public String edadBD { get; set; }

        public CUSTOMBeanDetalleBD()
        {
            lstDetalleBD = new List<BeanDetalleBD>();
            id_Auditoria = 0;
            ip_Servidor = "";
            nombreServidor = "";
            nombre_DB = "";
            size_DB = "";
            fechaCreacionDB = "";
            fechaUltimoUso = "";
            observacion = "";
            Antiguedad = "";
            TiempoDesuso = "";
            estado = "";
            edadBD = "";
        }
    }
}
