﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.BEANS
{
    public class ConfigBean
    {
        public String pagina { get; set; }
        public String filas { get; set; }
        public String valor { get; set; }

        public ConfigBean()
        {
            pagina = "1";
            filas = "10";
        }

    }
}
