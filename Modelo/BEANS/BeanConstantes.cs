﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo.BEANS
{
  public  class BeanConstantes
    {
   public const String strSessionActual = "SESSION_ACTUAL";

        public const String strPerfilOperador = "5";
        public const String strPerfilPT = "17";
        public const String strPerfilGerente = "10";
        public const String strPerfilConsultor = "1";
        public const String strPerfilCliente = "19";
        public const String strPerfilCoordinador = "2";
    }
}
