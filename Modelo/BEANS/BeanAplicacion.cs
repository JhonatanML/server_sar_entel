﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo.BEANS
{
    public class BeanAplicacion
    {
        public String idAplicacion { get; set; }
        public String consultor { get; set; }
        public String ruc { get; set; }
        public String razon_Social { get; set; }
        public String descripcion { get; set; }
        public String tecnologia { get; set; }
        public String version { get; set; }
        public String versionapk { get; set; }
        public String versionSuite { get; set; }
        public String versionSerMovil { get; set; }
        public String versionWeb { get; set; }
        public String urlB2B { get; set; }
        public String urlB2BSubString { get; set; }
        public String UrlServer { get; set; }
        public String UrlWeb { get; set; }
        public String UrlServerSubString { get; set; }
        public String fechaUltimaActualizacion { get; set; }
        public String estado { get; set; }
        public String TipoAplicacion { get; set; }
        public String contacto { get; set; }
        public String telefono { get; set; }
        public String ingeniero { get; set; }
        public Int32 idCliente { get; set; }

        public String codigoPadre { get; set; }
        public String perfil { get; set; }
        public String codigo { get; set; }
        public String detalle { get; set; }
        public Int32 idtecnologia { get; set; }
        public Int32 idEstadoDesarrollo { get; set; }
        public String estadodesarrollo { get; set; }
        public String cliente { get; set; }
        public String modulo { get; set; }
        public String zona { get; set; }

        public String nomconsultor { get; set; }
        public String zonaconsultor { get; set; }

        public String fecha { get; set; }
        public String detalles { get; set; }
        public String usuarioudpate { get; set; }

        public String idEstadoPlataforma { get; set; }
        public String hosting { get; set; }
        public String nombreDB { get; set; }
        public String idPlataforma { get; set; }
        public String idModulo { get; set; }
    }
}
