﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.BEANS
{
    public class BeanPaginate
    {
        public List<BeanExcepcion> lstResultadosExcepcion { get; set; }
        public List<BeanParametro> lstResultadosParametro { get; set; }
        public List<BeanCliente> lstResultadoBeanCliente { get; set; }
        public List<BeanAud_Reporte> lstResultadosAudReporte { get; set; }
        public List<BeanDetalleBD> lstResultadoBeanInstanciaEstandar { get; set; }
        public List<BeanDetalleBD> lstResultadoBeanInstanciaCustomizada { get; set; }
        public List<BeanServidores> lstResultadosServidores { get; set; }
        public List<BeanListBDProcesados> lstResultadosListProcesados { get; set; }
        public List<BeanUsuario> lstResultadoBeanUsuario { get; set; }
        public List<BeanAplicacion> lstResultadoBeanAplicacion { get; set; }
        public Int32 totalPages { get; set; }
        public Int32 totalRegistros { get; set; }

        public BeanPaginate()
        {
            lstResultadoBeanAplicacion = new List<BeanAplicacion>();
            lstResultadosExcepcion = new List<BeanExcepcion>();
            lstResultadosParametro = new List<BeanParametro>();
            lstResultadosAudReporte = new List<BeanAud_Reporte>();
            lstResultadoBeanInstanciaEstandar = new List<BeanDetalleBD>();
            lstResultadoBeanInstanciaCustomizada = new List<BeanDetalleBD>();
            lstResultadosServidores = new List<BeanServidores>();
            lstResultadosListProcesados = new List<BeanListBDProcesados>();
            totalPages = 0;
            totalRegistros = 0;
        }
    }
}
