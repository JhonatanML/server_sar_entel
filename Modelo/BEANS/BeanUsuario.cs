﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.BEANS
{
    public class BeanUsuario
    {
        public Int32 idUsuario { get; set; }
        public String id { get; set; }
        public String codigo { get; set; }
        public String nombre { get; set; }
        public String login { get; set; }
        public String clave { get; set; }
        public String nextel { get; set; }
        public String estado { get; set; }
        public String perfil { get; set; }
        public String zona { get; set; }
        public String usuario { get; set; }
        public String loginU { get; set; }
        public String passwordU { get; set; }
        public String codigoPerfil { get; set; }
        public String idZona { get; set; }
        public String id_perfil { get; set; }
        public String desEstado { get; set; }

        public BeanUsuario() { }
    }
}
