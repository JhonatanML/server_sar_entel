﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.BEANS
{
   public class BeanCliente
    {
        public String idCliente { get; set; }
        public String rucCliente { get; set; }
        public String codigo { get; set; }
        public String codigoPadre { get; set; }
        public String nombreComercial { get; set; }
        public String logoCliente { get; set; }
        public String razonSocial { get; set; }
        public String contacto { get; set; }
        public String telefono { get; set; }
        public String cargo { get; set; }
        public String zona { get; set; }
        public String idzona { get; set; }
        public String consultor { get; set; }
        public String idClienteIncidencia { get; set; }
        public String idIncidencia { get; set; }
        public Boolean estado { get; set; }
        public String loginU { get; set; }
        public String passCli { get; set; }
        public String servicio { get; set; }

        public BeanCliente() { }
    }
}
