﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.BEANS
{
    public class BeanAud_Reporte
    {
        public Int32 id_Auditoria { get; set; }
        public String ip_Servidor { get; set; }
        public String nombreServidor { get; set; }
        public String nombre_DB { get; set; }
        public String size_DB { get; set; }
        public String ownerDB { get; set; }
        public String fechaCreacionDB { get; set; }
        public String fechaUltimoUso { get; set; }
        public String observacion { get; set; }
        public String Antiguedad { get; set; }
        public String TiempoDesuso { get; set; }
        public String estado { get; set; }
        public String cliente { get; set; }
        public String modulo { get; set; }
        public String id_DB { get; set; }
        public String id_App { get; set; }
        public BeanAud_Reporte() { }
    }
}
