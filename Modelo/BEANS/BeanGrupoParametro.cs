﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.BEANS
{
    public class BeanGrupoParametro
    {
        public Int32 id_GrupoParametro { get; set; }
        public String nombre { get; set; }
        public String Descripcion { get; set; }
        public String Estado { get; set; }
    }
}
