﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.BEANS
{
  public  class BeanFileCarga
    {
        public String archivo { get; set; }
        public Int32 insertados { get; set; }
        public Int32 actualizados { get; set; }
        public Int32 subidos { get; set; }
        public Int32 total { get; set; }
        public String estado { get; set; }
        public String errorData { get; set; }
        public String errorExecute { get; set; }
        public String nombreArchivo { get; set; }
        public string EstadoToHTML
        {
            get
            {
                if (errorExecute != string.Empty)
                {
                    return "<a idC='" + archivo + "' class='btnError' style ='color:#8C0000'>Error en Archivo</a>";

                }
                else if (errorData != string.Empty)
                {

                    return "<a class='btnError' cod='" + archivo + "' style ='color:#FF8000'>Error en Datos</a>";

                }
                else
                {
                    return "<a idC='" + archivo + "' style ='color:#4B6F3F'>Ejecucion Exitosa</a>";

                }
            }
        }
        public BeanFileCarga()
        {
            archivo = "";
            insertados = 0;
            actualizados = 0;
            subidos = 0;
            total = 0;
            estado = "";
            errorData = "";
            errorExecute = "";
        }
    }
}
