﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo.BEANS
{
    public class Combo
    {
        public String Codigo { get; set; }
        public String Nombre { get; set; }

        public Combo()
        {
            Codigo = "";
            Nombre = "";
        }
    }
}
