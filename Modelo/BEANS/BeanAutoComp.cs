﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo.BEANS
{
    public class BeanAutoComp
    {
        public string vCodigo { get; set; }
        public string vNombre { get; set; }
        public string vNomCorto { get; set; }
    }
}
