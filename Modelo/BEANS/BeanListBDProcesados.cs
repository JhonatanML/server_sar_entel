﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.BEANS
{
    public class BeanListBDProcesados
    {
        public String ip_Server { get; set; }
        public String NOMBRE_SERVER { get; set; }
        public String NOMBRE_BD { get; set; }
        public String DESCRIPCION { get; set; }
        public String DESCRIPCIONCustom { get; set; }
        public String ESTADO { get; set; }

        public BeanListBDProcesados(){}

    }
}
