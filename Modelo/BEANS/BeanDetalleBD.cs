﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.BEANS
{
    public class BeanDetalleBD
    {
        public String cliente { get; set; }

        public String codigo_padre { get; set; }
        public String ruc { get; set; }
        public String razon_social { get; set; }
        public String modulo { get; set; }
        public String ruta_web { get; set; }
        public String url_descarga { get; set; }
        public String suite { get; set; }
        public String fecha_creacion { get; set; }
        public String fecha_ultimo { get; set; }
        public String desc_estado { get; set; }
        public String estado { get; set; }
        public String tecnologia { get; set; }
        public String id_tecnologia { get; set; }
        public String id_hosting { get; set; }
        public String fecha_creacion1 { get; set; }
        public String fecha_creacion2 { get; set; }

        public String plataforma { get; set; }
        public String aplicacion { get; set; }
        public String consultor { get; set; }
        public String zona { get; set; }
        public String responsable { get; set; }
        public String version_servidor_url { get; set; }
        public String version_movil_app_url { get; set; }
        public String fecha_registro { get; set; }
        public String fecha_actualizacion { get; set; }
        public String hosting { get; set; }
        public String version_suite{ get; set; }
        public String version_movil { get; set; }
        public String bd_name { get; set; }
        public String servicio { get; set; }
        public String ruta_mobile { get; set; }
        public String nombre_db { get; set; }

    }
}
