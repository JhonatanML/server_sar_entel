﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.BEANS
{
    public class BeanExcepcion
    {
        public Int32 idExcepciones { get; set; }
        public String nombreBD { get; set; }
        public String descripcion { get; set; }
        public String estado { get; set; }
    }
}
