﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.BEANS
{
    public class BeanParametro
    {
        public Int32 IdParametro { get; set; }
        public String Filtro { get; set; }
        public String nombre_tabla { get; set; }
        public String nombre_columna { get; set; }
        public String FechaRegistro { get; set; }
        public String Estado { get; set; }
        public Int32 id_GrupoParametro { get; set; }
        public String nombre_GrupoParametro { get; set; }
        
    }
}
