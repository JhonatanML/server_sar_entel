﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Utilidad;

namespace Modelo
{
    public class MTipoAtencion
    {
        public static DataTable ObtenerComboTipoAtencion()
        {
            ArrayList alParameters = new ArrayList();
            return SqlConnector.getDataTable("SAR_SPS_OBTENER_TIPO_ATENCION", alParameters);
        }
    }
}
