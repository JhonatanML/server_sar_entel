﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Utilidad;

namespace Modelo
{
    public class MRazonSocial
    {
        public static DataTable ObtenerRazonSocialBitacora(String psRazonSocial)
        {
            ArrayList loArrParametros = new ArrayList();
            SqlParameter loParametro;

            loParametro = new SqlParameter("@SEARCH", SqlDbType.VarChar, 100);
            loParametro.Value = psRazonSocial;
            loArrParametros.Add(loParametro);


            return SqlConnector.getDataTable("SAR_SPS_RAZONSOCIAL_BITACORA", loArrParametros);
        }
    }
}
