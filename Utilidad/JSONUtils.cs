﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;

namespace Utilidad
{
    public class JSONUtils
    {
        public static String serializeToJSON(Object obj)
        {   JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(obj);
        }

    }
}
